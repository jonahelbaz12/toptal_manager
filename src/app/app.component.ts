import { UserProvider } from './../providers/user/user';
import {Component, ViewChild} from '@angular/core';
import {Nav, NavController, Platform} from 'ionic-angular';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {AuthPage} from '../pages/auth/auth';
import {AngularFireAuth} from "@angular/fire/auth";
import {HomePage} from "../pages/home/home";
import { Network } from '@ionic-native/network';
import { ToastController } from 'ionic-angular';
import {PendingPage} from "../pages/pending/pending";
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  constructor (platform: Platform, statusBar: StatusBar,
               private splashScreen: SplashScreen, private fireAuth: AngularFireAuth,
               private network: Network, private toastCtrl: ToastController,
               private users: UserProvider) {
    platform.ready().then(() => {
      statusBar.styleDefault();
      this.trackAuth();
      this.trackNetwork();
    });
  }

  public trackNetwork() {
    let disconnectSubscription = this.network.onDisconnect().subscribe(() => {
      const toast = this.toastCtrl.create({
        message: 'Disconnected From the internet',
        duration: 3000
      });
      toast.present();
    });
  }

  public trackAuth () {
    this.fireAuth.auth.onAuthStateChanged(user => {
      this.splashScreen.hide();
      if (user) {
        if (JSON.parse(localStorage.getItem('@toptal:pending_status'))) {
          this.nav.setRoot(PendingPage);
        } else {
          this.users.getRange();
          this.nav.setRoot(HomePage);
        }
      } else {
        this.nav.setRoot(AuthPage);
      }
    });
  }
}

