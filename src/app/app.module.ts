import {BrowserModule} from '@angular/platform-browser';
import {CUSTOM_ELEMENTS_SCHEMA, ErrorHandler, NgModule} from '@angular/core';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';
import {SplashScreen} from '@ionic-native/splash-screen';
import {StatusBar} from '@ionic-native/status-bar';

import {MyApp} from './app.component';
import {AuthPage} from '../pages/auth/auth';
import {ComponentsModule} from '../components/components.module';
import {LoginPageModule} from '../pages/login/login.module';
import {SignUpPageModule} from '../pages/sign-up/sign-up.module';
import {AuthProvider} from '../providers/auth/auth';
import {firebase} from "../config/firebase";
import {AngularFireModule} from "@angular/fire";
import {AngularFireDatabaseModule} from "@angular/fire/database";
import {AngularFireAuthModule} from "@angular/fire/auth";
import {AngularFireStorageModule} from "@angular/fire/storage";
import {HomePageModule} from "../pages/home/home.module";
import {TasksProvider} from '../providers/tasks/tasks';
import {TimeProvider} from '../providers/time/time';
import {UserProvider} from '../providers/user/user';
import {Network} from '@ionic-native/network';
import {UserManagerProvider} from '../providers/user-manager/user-manager';
import {HttpClientModule} from "@angular/common/http";
import {SocialSharing} from '@ionic-native/social-sharing';
import {PendingPageModule} from "../pages/pending/pending.module";

@NgModule({
  declarations: [
    MyApp,
    AuthPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebase.config),
    AngularFireAuthModule,
    AngularFireDatabaseModule,
    AngularFireStorageModule,
    ComponentsModule,
    LoginPageModule,
    SignUpPageModule,
    HomePageModule,
    HttpClientModule,
    PendingPageModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AuthPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthProvider,
    TasksProvider,
    TimeProvider,
    UserProvider,
    Network,
    UserManagerProvider,
    SocialSharing
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class AppModule {
}
