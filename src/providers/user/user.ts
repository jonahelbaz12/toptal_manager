import {EventEmitter, Injectable} from '@angular/core';
import {AngularFireAuth} from "@angular/fire/auth";
import {AngularFireDatabase} from "@angular/fire/database";
import {Task} from "../../models/task";
import {Roles} from '../../models/roles';
import {toptalStrings} from "../../config/config";

@Injectable()
export class UserProvider {
  public dateStringFrom: string  = "";
  public dateStringTo: string = "";
  public role: Roles = Roles.USER;
  public navigatePending: EventEmitter<boolean> = new EventEmitter<boolean>();
  constructor(private fireAuth: AngularFireAuth, private fireDb: AngularFireDatabase) {}

  public fetchRole(): Promise<any> {
    const current = this.fireAuth.auth.currentUser;
    if (current) {
      return this.fireDb.database.ref('roles/' + current.uid).once('value', snapshot => snapshot).then(snapshot => {
        this.setRole(snapshot.val());
        return this.role;
      });
    } else {
      alert('Not Authenticated');
    }
  }

  public getRange() {
    const current = this.fireAuth.auth.currentUser;
    if (current) {
      this.fireDb.database.ref('users/' + current.uid + '/range').once('value', snapshot => {
        this.dateStringTo = snapshot.child('to').val() || "";
        this.dateStringFrom = snapshot.child('from').val() || "";
      });
    } else {
      alert('Not Authenticated');
    }
  }

  public writeNewRange(from: string, to: string): void {
    const current = this.fireAuth.auth.currentUser;
    if (current) {
      this.dateStringTo = to;
      this.dateStringFrom  = from;
      this.fireDb.database.ref('users/' + current.uid + '/range').update({
        from: this.dateStringFrom,
        to: this.dateStringTo
      });
    } else {
      alert('Not Authenticated');
    }
  }

  public isInWorkHours(task: Task): boolean {
    return task.time >= this.dateStringFrom && task.time <= this.dateStringTo
  }

  public evaluateCustomWorkHours(task: Task, workHours: any) {
    return task.time >= workHours.from && task.time <= workHours.to
  }

  public getCustomWorkHours(uid: string): Promise<any> {
    return this.fireDb.database.ref('users/' + uid + '/range').once('value', s => s).then(snapshot => {
      return {
        from: snapshot.child('from').val() || "",
        to: snapshot.child('to').val() || ""
      }
    });
  }

  public observeUserProvider() {
    const uid = this.fireAuth.auth.currentUser.uid;
    const ref = this.fireDb.database.ref('roles/' + uid);
    ref.on('value', snapshot => {
      if (snapshot.exists()) {
        if (snapshot.val() === 'rejected') {
          alert(toptalStrings.rejectedStatus);
          localStorage.clear();
          this.navigatePending.emit(false);
        } else {
          this.navigatePending.emit(true);
        }
        ref.off();
        localStorage.removeItem('@toptal:pending_status');
      }
    });
  }

  private setRole(role) {
    switch(role) {
      case 'manager':
          this.role = Roles.MANAGER;
          return;
      case 'admin':
          this.role = Roles.ADMIN;
          return;
      default:
          this.role = Roles.USER

    }
  }


}
