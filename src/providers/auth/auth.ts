import {Injectable} from '@angular/core';
import {AuthObj} from "../../models/signUpObj";
import {storage} from "../../config/storage";
import {User} from "../../models/user";
import {AngularFireAuth} from "@angular/fire/auth";
import {AngularFireDatabase} from "@angular/fire/database";
import {shards} from '../../config/firebase';
import {toptalStrings} from "../../config/config";

@Injectable()
export class AuthProvider {
  constructor(private fireAuth: AngularFireAuth, private fireDb: AngularFireDatabase) {
  }

  static getLocalUser(): User {
    if (window.localStorage.getItem(storage.local_user) !== null) {
      return (JSON.parse(window.localStorage.getItem(storage.local_user)) || []);
    }
    return null;
  }

  public validateSignUpData(data: AuthObj): boolean {
    if (data.password !== data.repassword) {
      alert(this.constants.passwordsDontMatch);
      return false;
    }
    return true;
  }

  public signUp(data: AuthObj): Promise<any> {
    if (data.admin) {
      localStorage.setItem('@toptal:pending_status', JSON.stringify(true));
    }
    return this.fireAuth.auth.createUserWithEmailAndPassword(data.email, data.password)
      .then(res => {
        this.createUser(data.email, data.admin);
      })
      .catch(error => {
        localStorage.removeItem('@toptal:pending_status');
      alert(error.message);
    });
  }

  public attemptLogin(data: any): Promise<any> {
    return this.fireAuth.auth.signInWithEmailAndPassword(data.email, data.password)
      .then(() => {
        return;
      }).catch(error => {
      alert(error.message);
      return;
    });
  }

  public createUser(email: string, admin: string): Promise<any> {
    const user_id = this.fireAuth.auth.currentUser.uid;
    const obj = {
      email: email,
      admin: admin,
      created_at: new Date().getTime()
    };
    if (obj.admin === "") delete obj.admin;
    return this.fireDb.database.ref('users/' + user_id).set(obj)
      .then(() => {
      const user: User = {
        uid: user_id,
        email: email
      };
      localStorage.setItem('@toptal:user', JSON.stringify(user));
    });
  }

  public logout(): Promise<any> {
    localStorage.clear();
    return this.fireAuth.auth.signOut();
  }

  get constants (): any {
    return toptalStrings;
  }

}
