import {Injectable} from '@angular/core';

@Injectable()
export class TimeProvider {

  constructor() {
  }

  static getTimeOfDayFromTimeString(dateString: string): string {
    return dateString.substr(11, 5);
  }

}
