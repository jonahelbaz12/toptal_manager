import {Injectable, EventEmitter} from '@angular/core';
import {toptalStrings} from "../../config/config";
import {Task} from '../../models/task';
import {AngularFireAuth} from "@angular/fire/auth";
import {AngularFireDatabase} from "@angular/fire/database";
import {TimeProvider} from "../time/time";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {GroupedTasks} from "../../models/groupedTasks";
import {Observable} from "rxjs/Observable";
import {HttpClient} from "@angular/common/http";
import {map} from "rxjs/operators";

@Injectable()
export class TasksProvider {
  public groupedTasks: BehaviorSubject<GroupedTasks[]> = new BehaviorSubject<GroupedTasks[]>([]);
  public submittedNewTask: EventEmitter<any> = new EventEmitter<any>();
  constructor (private fireAuth: AngularFireAuth, private fireDb: AngularFireDatabase,
               private httpClient: HttpClient) {
  }

  public processIncomingTasks (): any {
    const current = this.fireAuth.auth.currentUser;
    if (current) {
      this.fireDb.database.ref('tasks/' + current.uid).on('value', snapshot => {
        const newTaskSet: Task[] = [];
        snapshot.forEach(snap => {
          newTaskSet.push({
            description: snap.child('description').val(),
            hours: snap.child('hours').val(),
            uid: snap.key,
            dateString: snap.child('dateString').val(),
            time: TimeProvider.getTimeOfDayFromTimeString(snap.child('dateString').val())
          });
          return false;
        });
        const sortedGroupTasks = this.sortAndGroupTasks(newTaskSet);
        this.groupedTasks.next(sortedGroupTasks);
      });
    } else {
      alert('Not Authenticated');
    }
  }

  public validateTaskEntry (task: Task): boolean {
    if (task.description === "") {
      alert(this.constants.emptyDesc);
      return false;
    }
    return true;
  }

  public submitNewTask (task: Task, uid: string): Promise<any> {
    return this.fireDb.database.ref('tasks/' + uid).push(task).then(() => {
      this.submittedNewTask.emit();
    });
  }

  public deleteEntry (entryId: string, uid: string): Promise<any> {
    return this.fireDb.database.ref('tasks/' + uid + '/' + entryId).remove().then(() => {
      this.submittedNewTask.emit();
    });
  }

  public updateTask (task: Task, uid: string): Promise<any> {
    return this.fireDb.database.ref('tasks/' + uid + '/' + task.uid).update(task).then(() => {
      this.submittedNewTask.emit();
    });
  }

  public getGroupedTasksById (uid: string): Promise<GroupedTasks[]> {
    return this.fireDb.database.ref('tasks/' + uid).once('value', s => s).then(snapshot => {
      const newTaskSet: Task[] = [];
      snapshot.forEach(snap => {
        newTaskSet.push({
          description: snap.child('description').val(),
          hours: snap.child('hours').val(),
          uid: snap.key,
          dateString: snap.child('dateString').val(),
          time: TimeProvider.getTimeOfDayFromTimeString(snap.child('dateString').val())
        });
        return false;
      });
      return this.sortAndGroupTasks(newTaskSet);
    });
  }

  public getExportedHtml(list: GroupedTasks[]): Observable<any> {
    return this.httpClient.post('http://jonahelbaz.pythonanywhere.com//transform', {jsonToParse: list})
      .pipe(map(data => data));
  }

  private sortAndGroupTasks (tasks: Task[]): GroupedTasks[] {
    tasks = tasks.sort((a, b) => {
      const epochA = new Date(a.dateString).getTime();
      const epochB = new Date(b.dateString).getTime();
      return epochB - epochA;
    });
    const groups: GroupedTasks[] = [];
    tasks.forEach(task => {
      const subDateString = task.dateString.substr(0, 10);
      const i = groups.findIndex(o => o.titledDate === subDateString);
      if (i === -1) {
        groups.push({
          titledDate: subDateString,
          tasks: [task]
        });
      } else {
        groups[i].tasks.push(task);
      }
    });
    return groups;
  }

  get constants () {
    return toptalStrings;
  }

}
