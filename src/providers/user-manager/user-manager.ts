import {Injectable} from '@angular/core';
import {User} from "../../models/user";
import {BehaviorSubject} from 'rxjs';
import {AngularFireDatabase} from "@angular/fire/database";
import {AlertController} from 'ionic-angular';
import {toptalStrings} from "../../config/config";
import {AngularFireAuth} from '@angular/fire/auth';
import {firebase as fireConfig} from "../../config/firebase";
import * as firebase from 'firebase';
import {UserProvider} from "../user/user";
import {Roles} from "../../models/roles";

@Injectable()
export class UserManagerProvider {
  public users: BehaviorSubject<User[]> = new BehaviorSubject<User[]>([]);

  constructor (private fireDb: AngularFireDatabase, private fireAuth: AngularFireAuth,
               private userProvider: UserProvider) {
  }

  public createNewUser (email: string, password: string) {
    const secondaryApp = firebase.initializeApp(fireConfig.config, "Secondary");
    return secondaryApp.auth().createUserWithEmailAndPassword(email, password)
      .then(res => {
        secondaryApp.auth().signOut();
        secondaryApp.delete();
        return this.fireDb.database.ref('users/' + res.user.uid).set({
          email: email,
          created_at: new Date().getTime()
        });
      }).catch(error => {
        alert(error.message);
      });
  }

  public fetchUsers () {
    this.fireDb.database.ref('users').on('value', snapshot => {
      const users: User[] = [];
      snapshot.forEach(snap => {
        users.push({
          email: snap.child('email').val(),
          uid: snap.key
        });
      });
      this.users.next(users);
    });
  }

  public attemptToDeleteUser (user: User, alertCtrl: AlertController) {
    const current = this.fireAuth.auth.currentUser;
    if (current.uid === user.uid) {
      this.alertError(alertCtrl, this.constants.cantDeleteYourself);
      return;
    }
    this.doesUserHaveRole(user.uid).then(res => {
      if (res === null || res === undefined) {
        this.deleteAllUserEntries(user);
        return;
      }
      if (this.userProvider.role === Roles.MANAGER && res !== null) {
          this.alertError(alertCtrl, this.constants.cantDeleteUserWithStatus);
          return;
      }
      if (this.userProvider.role === Roles.ADMIN && res === 'admin') {
        this.alertError(alertCtrl, this.constants.cantDeleteUserWithStatus);
        return;
      }
      this.deleteAllUserEntries(user);
    });
  }

  private deleteAllUserEntries (user: User): Promise<any> {
    return this.fireDb.database.ref('tasks/' + user.uid).remove().then(() => {
      this.fireDb.database.ref('users/' + user.uid).remove();
    });
  }

  private doesUserHaveRole (uid: string): Promise<any> {
    return this.fireDb.database.ref('roles/' + uid).once('value', snap => snap).then(snapshot => {
      return snapshot.val();
    });
  }

  private alertError (alertCtrl: AlertController, subTitle: string): void {
    const alert = alertCtrl.create({
      title: this.constants.oops,
      subTitle: subTitle,
      buttons: [
        {
          text: 'OK',
        }
      ]
    });
    alert.present();
  }

  get constants (): any {
    return toptalStrings;
  }


}
