export interface User {
  email: string,
  uid: string,
  created_at?: number
}
