export interface Task {
  description: string,
  hours: number,
  time?: string,
  uid?: string,
  dateString: string
}
