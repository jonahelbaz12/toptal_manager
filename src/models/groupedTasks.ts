import {Task} from "./task";

export interface GroupedTasks {
  titledDate: string,
  tasks: Task[]
}
