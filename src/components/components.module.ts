import { NgModule } from '@angular/core';
import { SegmentComponent } from './segment/segment';
import {BrowserModule} from '@angular/platform-browser';
import {CommonModule} from '@angular/common';
import {IonicModule} from 'ionic-angular';
import {MyApp} from '../app/app.component';
@NgModule({
	declarations: [SegmentComponent],
	imports: [
    BrowserModule,
    CommonModule,
    IonicModule.forRoot(MyApp),
  ],
	exports: [SegmentComponent]
})
export class ComponentsModule {}
