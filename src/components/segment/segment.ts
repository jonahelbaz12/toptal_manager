import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'segment',
  templateUrl: 'segment.html'
})
export class SegmentComponent {
  @Input() leftVal: string;
  @Input() rightVal: string;
  @Output() leftClicked: EventEmitter<any> = new EventEmitter<any>();
  @Output() rightClicked: EventEmitter<any> = new EventEmitter<any>();
  public leftValOn = true;
  constructor() {
  }

  public clickedRight() {
    this.leftValOn = false;
    this.rightClicked.emit(null);
  }

  public clickedLeft() {
    this.leftValOn = true;
    this.leftClicked.emit(null);
  }

}
