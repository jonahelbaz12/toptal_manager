export const firebase = {
  config: {
    apiKey: "AIzaSyCLtfPOAoCKr-vtkCMOx3Nvl8wXHQpz3I4",
    authDomain: "toptal-exam.firebaseapp.com",
    databaseURL: "https://toptal-exam.firebaseio.com",
    projectId: "toptal-exam",
    storageBucket: "toptal-exam.appspot.com",
    messagingSenderId: "978362305440"
  },
};

export const shards = {
  users: "https://toptal-exam-users.firebaseio.com/",
  tasks: "https://toptal-exam-tasks.firebaseio.com/",
  main: "https://toptal-exam.firebaseio.com/"
};
