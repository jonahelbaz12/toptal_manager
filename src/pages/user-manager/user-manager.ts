import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {AlertController, IonicPage, NavController} from 'ionic-angular';
import {UserManagerProvider} from "../../providers/user-manager/user-manager";
import {toptalStrings} from "../../config/config";
import {User} from "../../models/user";


@IonicPage()
@Component({
  selector: 'page-user-manager',
  templateUrl: 'user-manager.html',
})
export class UserManagerPage implements OnInit {

  constructor(public navCtrl: NavController, public userManager: UserManagerProvider,
              private cd: ChangeDetectorRef, private alertCtrl: AlertController) {
  }

  ngOnInit() {
    this.userManager.fetchUsers();
    this.userManager.users.subscribe(() => {
      if (!this.cd['destroyed']) {
        this.cd.detectChanges();
      }
    });
  }

  editEntry(user: User): void {
    const alert = this.alertCtrl.create({
      title: this.constants.oops,
      subTitle: this.constants.nothingToEdit,
      buttons: [
        {
          text: 'OK',
        }
      ]
    });
    alert.present();
  }

  deleteEntry(user: User): void {
    this.userManager.attemptToDeleteUser(user, this.alertCtrl);
  }

  get users() {
    return this.userManager.users.getValue();
  }

  get constants (): any {
    return toptalStrings;
  }

}
