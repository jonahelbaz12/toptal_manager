import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {AlertController, IonicPage, NavController, ModalController} from 'ionic-angular';
import {UserManagerProvider} from "../../providers/user-manager/user-manager";
import {toptalStrings} from "../../config/config";
import {User} from "../../models/user";
import {AdminTaskViewPage} from "../admin-task-view/admin-task-view";


@IonicPage()
@Component({
  selector: 'page-admin-manager',
  templateUrl: 'admin-manager.html',
})
export class AdminManagerPage {

  constructor(public navCtrl: NavController, public userManager: UserManagerProvider,
              private cd: ChangeDetectorRef, private alertCtrl: AlertController,
              private modalCtrl: ModalController) {
  }

  ngOnInit() {
    this.userManager.fetchUsers();
    this.userManager.users.subscribe(() => {
      if (!this.cd['destroyed']) {
        this.cd.detectChanges();
      }
    });
  }

  public editEntry(user: User): void {
    const alert = this.alertCtrl.create({
      title: this.constants.oops,
      subTitle: this.constants.nothingToEdit,
      buttons: [
        {
          text: 'OK',
        }
      ]
    });
    alert.present();
  }

  public deleteEntry(user: User): void {
    const alert = this.alertCtrl.create({
      title: this.constants.deleting,
      subTitle: this.constants.areYouSure,
      buttons: [
        {
          text: 'Cancel'
        },
        {
          text: 'Yes',
          handler: () => {
            this.userManager.attemptToDeleteUser(user, this.alertCtrl);
          }
        }
      ]
    });
    alert.present();
  }

  public viewUserTasks(user: User): void {
    const modal = this.modalCtrl.create(AdminTaskViewPage, {user: user});
    modal.present();
  }

  get users() {
    return this.userManager.users.getValue();
  }

  get constants (): any {
    return toptalStrings;
  }
}
