import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdminManagerPage } from './admin-manager';
import {AdminTaskViewPage} from "../admin-task-view/admin-task-view";
import {AdminTaskViewPageModule} from "../admin-task-view/admin-task-view.module";

@NgModule({
  declarations: [
    AdminManagerPage,
  ],
  imports: [
    IonicPageModule.forChild(AdminManagerPage),
    AdminTaskViewPageModule
  ],
  exports: [
    AdminManagerPage
  ]
})
export class AdminManagerPageModule {}
