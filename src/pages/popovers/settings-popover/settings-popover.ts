import {ChangeDetectorRef, Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {AuthProvider} from "../../../providers/auth/auth";
import {toptalStrings} from "../../../config/config";
import {TasksProvider} from "../../../providers/tasks/tasks";
import {SocialSharing} from '@ionic-native/social-sharing';

@IonicPage()
@Component({
  selector: 'page-settings-popover',
  templateUrl: 'settings-popover.html',
})
export class SettingsPopoverPage {
  public loading: boolean = false;
  public allowHtml: boolean = false;
  constructor(private navCtrl: NavController, private auth: AuthProvider,
              private navParams: NavParams, private tasks: TasksProvider,
              private social: SocialSharing, private cd: ChangeDetectorRef) {
  }

  ionViewDidLoad() {
    this.allowHtml = this.navParams.get('allowHtml');

  }

  public logout(): void {
    this.navCtrl.pop();
    this.auth.logout();
  }

  public exportAs() {
    this.loading = true;
    this.registerChanges();
    const list = this.navParams.get('list');
    this.tasks.getExportedHtml(list).subscribe(res => {
      this.loading = false;
      const options = {
        message: 'Here is an html copy of your logged tasks!',
        subject: 'HTML of Logged Tasks',
        files: [res], // an array of filenames either locally or remotely
        // url: 'https://www.website.com/foo/#bar?a=b',
      };
      this.social.shareWithOptions(options).then(() => {
        this.registerChanges();
      })
    }, err => {
      this.loading = false;
      this.registerChanges();
      alert(JSON.stringify(err));
    });
  }

  private registerChanges() {
    if (!this.cd['destroyed']) {
      this.cd.detectChanges();
    }
  }

  get constants (): any {
    return toptalStrings;
  }

}
