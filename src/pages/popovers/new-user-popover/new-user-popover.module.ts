import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NewUserPopoverPage } from './new-user-popover';

@NgModule({
  declarations: [
    NewUserPopoverPage,
  ],
  imports: [
    IonicPageModule.forChild(NewUserPopoverPage),
  ],
})
export class NewUserPopoverPageModule {}
