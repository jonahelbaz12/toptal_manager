import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {toptalStrings} from "../../../config/config";
import {UserManagerProvider} from "../../../providers/user-manager/user-manager";

@IonicPage()
@Component({
  selector: 'page-new-user-popover',
  templateUrl: 'new-user-popover.html',
})
export class NewUserPopoverPage {
  public tempPass: string = "";
  public email: string = "";

  constructor (public navCtrl: NavController, public userManager: UserManagerProvider) {
  }

  submit () {
    if (this.email === '') {
      alert('Please leave no empty fields');
      return;
    }
    if (this.tempPass.length < 8) {
      alert('Password must be at least 8 character');
      return;
    }
    this.userManager.createNewUser(this.email, this.tempPass);
    this.navCtrl.pop();
  }

  get constants (): any {
    return toptalStrings;
  }

}
