import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditTaskPopoverPage } from './edit-task-popover';

@NgModule({
  declarations: [
    EditTaskPopoverPage,
  ],
  imports: [
    IonicPageModule.forChild(EditTaskPopoverPage),
  ],
})
export class EditTaskPopoverPageModule {}
