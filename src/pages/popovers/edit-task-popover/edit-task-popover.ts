import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {toptalStrings} from "../../../config/config";
import {TasksProvider} from "../../../providers/tasks/tasks";
import {Task} from "../../../models/task";


@IonicPage()
@Component({
  selector: 'page-edit-task-popover',
  templateUrl: 'edit-task-popover.html',
})
export class EditTaskPopoverPage {
  public newTask: Task = {
    description: "",
    hours: 1,
    dateString: "",
  };
  public loading: boolean = false;
  constructor(private navCtrl: NavController, private navParams: NavParams,
              private tasks: TasksProvider) {
  }

  ionViewDidLoad() {
    this.newTask = this.navParams.get('task');
  }

  public submit(): void {
    if (this.tasks.validateTaskEntry(this.newTask)) {
      this.loading = true;
      this.tasks.updateTask(this.newTask, this.navParams.get('uid')).then(() => {
        this.loading = false;
        this.navCtrl.pop();
      });
    }
  }

  get constants () {
    return toptalStrings;
  }

}
