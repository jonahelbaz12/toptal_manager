import {Component} from '@angular/core';
import {IonicPage} from 'ionic-angular';
import {toptalStrings} from "../../../config/config";
import {UserProvider} from "../../../providers/user/user";

@IonicPage()
@Component({
  selector: 'page-preferences-popover',
  templateUrl: 'preferences-popover.html',
})
export class PreferencesPopoverPage {
  public dateStringFrom: string;
  public dateStringTo: string;
  constructor(public users: UserProvider) {
    this.dateStringFrom = this.users.dateStringFrom;
    this.dateStringTo = this.users.dateStringTo;
  }


  public valuesChanged(): void {
    if ( this.dateStringFrom !== "" && this.dateStringTo !== "") {
      this.users.writeNewRange(this.dateStringFrom, this.dateStringTo);
    }
  }

  get constants (): any {
    return toptalStrings;
  }
}
