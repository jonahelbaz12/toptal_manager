import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PreferencesPopoverPage } from './preferences-popover';

@NgModule({
  declarations: [
    PreferencesPopoverPage,
  ],
  imports: [
    IonicPageModule.forChild(PreferencesPopoverPage),
  ],
})
export class PreferencesPopoverPageModule {}
