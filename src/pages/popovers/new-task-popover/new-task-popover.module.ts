import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NewTaskPopoverPage } from './new-task-popover';

@NgModule({
  declarations: [
    NewTaskPopoverPage,
  ],
  imports: [
    IonicPageModule.forChild(NewTaskPopoverPage),
  ],
})
export class NewTaskPopoverPageModule {}
