import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {toptalStrings} from "../../../config/config";
import {TasksProvider} from "../../../providers/tasks/tasks";
import {Task} from '../../../models/task';

@IonicPage()
@Component({
  selector: 'page-new-task-popover',
  templateUrl: 'new-task-popover.html',
})
export class NewTaskPopoverPage {
  public newTask: Task = {
    description: "",
    hours: 1,
    dateString: "",
  };
  public loading: boolean = false;
  constructor(private navCtrl: NavController, private tasks: TasksProvider,
              private navParams: NavParams) {
  }

  public submit(): void {
    if (this.tasks.validateTaskEntry(this.newTask)) {
      this.loading = true;
      this.tasks.submitNewTask(this.newTask, this.navParams.get('uid')).then(() => {
        this.loading = false;
        this.navCtrl.pop();
      });
    }
  }

  get constants (): any {
    return toptalStrings;
  }

}
