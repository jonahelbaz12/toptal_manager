import {Component, EventEmitter, Output} from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {toptalStrings} from '../../config/config';
import {AuthObj} from "../../models/signUpObj";

@IonicPage()
@Component({
  selector: 'page-sign-up',
  templateUrl: 'sign-up.html',
})
export class SignUpPage {
  public email: string = "";
  public password: string = "";
  public repassword: string = "";
  public admin: string = "";
  @Output() buttonClick: EventEmitter<AuthObj> = new EventEmitter<AuthObj>();

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  public clickedSignUp(): void {
    this.buttonClick.emit({
      email: this.email,
      password: this.password,
      repassword: this.repassword,
      admin: this.admin
    });
  }

  get constants(): any {
    return toptalStrings;
  }
}
