import {Component} from '@angular/core';
import {IonicPage, NavController, PopoverController} from 'ionic-angular';
import {toptalStrings} from "../../config/config";
import {SettingsPopoverPage} from "../popovers/settings-popover/settings-popover";
import {UserProvider} from "../../providers/user/user";
import {HomePage} from "../home/home";
import {AuthPage} from "../auth/auth";

@IonicPage()
@Component({
  selector: 'page-pending',
  templateUrl: 'pending.html',
})
export class PendingPage {

  constructor(public navCtrl: NavController, public popoverCtrl: PopoverController,
              private userProvider: UserProvider) {
    this.userProvider.navigatePending.subscribe(res => {
      this.navCtrl.setRoot(HomePage);
    });
    this.userProvider.observeUserProvider();
  }


  public openSettings (myEvent: any): void {
    const popover = this.popoverCtrl.create(SettingsPopoverPage, {
      list: null,
      allowHtml: false
    }, {cssClass: 'settings-popover'});
    popover.present({
      ev: myEvent
    });
  }

  get constants (): any {
    return toptalStrings;
  }


}
