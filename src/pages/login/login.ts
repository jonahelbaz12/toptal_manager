import {Component, EventEmitter, Output} from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {toptalStrings} from '../../config/config';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  public email: string = "";
  public password: string = "";
  @Output() buttonClick: EventEmitter<any> = new EventEmitter<any>();
  constructor() {
  }

  get constants(): any {
    return toptalStrings;
  }

}
