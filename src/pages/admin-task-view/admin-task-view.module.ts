import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdminTaskViewPage } from './admin-task-view';

@NgModule({
  declarations: [
    AdminTaskViewPage,
  ],
  imports: [
    IonicPageModule.forChild(AdminTaskViewPage),
  ],
})
export class AdminTaskViewPageModule {}
