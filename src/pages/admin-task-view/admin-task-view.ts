import {AlertController, IonicPage, PopoverController, NavParams, NavController} from 'ionic-angular';
import {GroupedTasks} from "../../models/groupedTasks";
import {Task} from "../../models/Task";
import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {UserProvider} from "../../providers/user/user";
import {EditTaskPopoverPage} from "../popovers/edit-task-popover/edit-task-popover";
import {TasksProvider} from "../../providers/tasks/tasks";
import {toptalStrings} from "../../config/config";
import {User} from "../../models/user";
import {NewTaskPopoverPage} from "../popovers/new-task-popover/new-task-popover";
import {AngularFireAuth} from '@angular/fire/auth';
import {SettingsPopoverPage} from "../popovers/settings-popover/settings-popover";
import {Roles} from "../../models/roles";

@IonicPage()
@Component({
  selector: 'page-admin-task-view',
  templateUrl: 'admin-task-view.html',
})
export class AdminTaskViewPage {
  public user: User;
  public groupedTasks: GroupedTasks[] = [];
  public loading: boolean = false;
  public workHours = {
    from: "",
    to: ""
  };
  constructor (private users: UserProvider, private navParams: NavParams,
               private taskProvider: TasksProvider, private popoverCtrl: PopoverController,
               private alertCtrl: AlertController, private cd: ChangeDetectorRef,
               private navCtrl: NavController, private fireAuth: AngularFireAuth) {
  }

  ngOnInit () {
    this.groupedTasks = [];
    this.user = this.navParams.get('user');
    this.subscribeToTasks();
    this.users.getCustomWorkHours(this.user.uid).then(res => {
      this.workHours = res;
      if (!this.cd['destroyed']) {
        this.cd.detectChanges();
      }
    });
    this.taskProvider.submittedNewTask.subscribe(() => {
      this.subscribeToTasks();
    });
  }

  public dismiss() {
    this.navCtrl.pop();
  }

  public openSettings (myEvent: any): void {
    const popover = this.popoverCtrl.create(SettingsPopoverPage, {
      list: this.groupedTasks,
      allowHtml: true
    }, {cssClass: 'settings-popover'});
    popover.present({
      ev: myEvent
    });
  }

  public isInWorkHours (task: Task): boolean {
    return this.users.evaluateCustomWorkHours(task, this.workHours);
  }

  public editEntry (task: Task): void {
    const popover = this.popoverCtrl.create(EditTaskPopoverPage, {task: task, uid: this.user.uid}, {cssClass: 'new-task-popover'});
    popover.present();
  }

  public deleteEntry (task: Task): void {
    const alert = this.alertCtrl.create({
      title: this.constants.deleteTitle,
      subTitle: this.constants.deleteConfirm,
      buttons: [
        {
          text: 'Cancel',
        },
        {
          text: 'OK',
          handler: data => {
            this.taskProvider.deleteEntry(task.uid, this.user.uid);
          }
        }
      ]
    });
    alert.present();
  }

  private subscribeToTasks (): void {
    this.loading = true;
    this.taskProvider.getGroupedTasksById(this.user.uid).then(tasks => {
      this.loading = false;
      this.groupedTasks = tasks;
      if (!this.cd['destroyed']) {
        this.cd.detectChanges();
      }
    });
  }

  public createNewTask (): void {
    const popover = this.popoverCtrl.create(NewTaskPopoverPage, {uid: this.user.uid}, {cssClass: 'new-task-popover'});
    popover.present();
  }

  get myGroupedTasks (): GroupedTasks[] {
    return this.groupedTasks;
  }

  get constants (): any {
    return toptalStrings;
  }

}
