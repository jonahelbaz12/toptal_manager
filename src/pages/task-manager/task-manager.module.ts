import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TaskManagerPage } from './task-manager';

@NgModule({
  declarations: [
    TaskManagerPage,
  ],
  imports: [
    IonicPageModule.forChild(TaskManagerPage),
  ],
  exports: [
    TaskManagerPage
  ]
})
export class TaskManagerPageModule {}
