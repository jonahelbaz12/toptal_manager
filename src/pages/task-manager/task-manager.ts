import {AlertController, IonicPage, PopoverController} from 'ionic-angular';
import {GroupedTasks} from "../../models/groupedTasks";
import {Task} from "../../models/Task";
import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {UserProvider} from "../../providers/user/user";
import {EditTaskPopoverPage} from "../popovers/edit-task-popover/edit-task-popover";
import {TasksProvider} from "../../providers/tasks/tasks";
import {toptalStrings} from "../../config/config";
import {AngularFireAuth} from '@angular/fire/auth';


@IonicPage()
@Component({
  selector: 'page-task-manager',
  templateUrl: 'task-manager.html',
})
export class TaskManagerPage implements OnInit {
  constructor (private users: UserProvider,
               private taskProvider: TasksProvider, private popoverCtrl: PopoverController,
               private alertCtrl: AlertController, private cd: ChangeDetectorRef,
               private fireAuth: AngularFireAuth) {
  }

  ngOnInit () {
    this.subscribeToTasks();
  }

  public isInWorkHours (task: Task): boolean {
    return this.users.isInWorkHours(task);
  }

  public editEntry (task: Task): void {
    const uid = this.fireAuth.auth.currentUser.uid;
    const popover = this.popoverCtrl.create(EditTaskPopoverPage, {task: task, uid: uid}, {cssClass: 'new-task-popover'});
    popover.present();
  }

  public deleteEntry (task: Task): void {
    const uid = this.fireAuth.auth.currentUser.uid;
    const alert = this.alertCtrl.create({
      title: this.constants.deleteTitle,
      subTitle: this.constants.deleteConfirm,
      buttons: [
        {
          text: 'Cancel',
        },
        {
          text: 'OK',
          handler: data => {
            this.taskProvider.deleteEntry(task.uid, uid);
          }
        }
      ]
    });
    alert.present();
  }

  private subscribeToTasks (): void {
    this.taskProvider.groupedTasks.subscribe(() => {
      if (!this.cd['destroyed']) {
        this.cd.detectChanges();
      }
    });
  }

  get myGroupedTasks (): GroupedTasks[] {
    return this.taskProvider.groupedTasks.getValue();
  }

  get constants (): any {
    return toptalStrings;
  }

}
