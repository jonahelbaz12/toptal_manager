import {ChangeDetectorRef, Component} from '@angular/core';
import {IonicPage} from 'ionic-angular';
import {toptalStrings} from '../../config/config';
import {AuthObj} from "../../models/signUpObj";
import {AuthProvider} from "../../providers/auth/auth";

@IonicPage()
@Component({
  selector: 'page-auth',
  templateUrl: 'auth.html',
})
export class AuthPage {
  public login = true;
  public loading: boolean = false;
  constructor (private authProvider: AuthProvider, private cd: ChangeDetectorRef) {
  }

  public loginClicked (): void {
    this.login = true;
    if (!this.cd['destroyed']) {
      this.cd.detectChanges();
    }
  }

  public signupClicked (): void {
    this.login = false;
    if (!this.cd['destroyed']) {
      this.cd.detectChanges();
    }
  }

  public attemptLogin (data: any): void {
    if (data.email === '' || data.password === '') {
     alert('Please leave no empty fields');
     return;
    }
    this.loading = true;
    if (!this.cd['destroyed']) {
      this.cd.detectChanges();
    }
    this.authProvider.attemptLogin(data).then(() => {
      this.loading = false;
      if (!this.cd['destroyed']) {
        this.cd.detectChanges();
      }
    });
  }

  public attempSignup (data: AuthObj): void {
    if (this.authProvider.validateSignUpData(data)) {
      this.loading = true;
      this.authProvider.signUp(data).then(() => {
        this.loading = false;
      }, () => {
        this.loading = false;
      });
    }
  }

  get constants (): any {
    return toptalStrings;
  }

}
