import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AuthPage } from './auth';
import {ComponentsModule} from '../../components/components.module';
import {LoginPageModule} from '../login/login.module';

@NgModule({
  declarations: [
    AuthPage,
  ],
  imports: [
    IonicPageModule.forChild(AuthPage),
    ComponentsModule
  ]
})
export class AuthPageModule {}
