import {ChangeDetectorRef, Component} from '@angular/core';
import {IonicPage, PopoverController} from 'ionic-angular';
import {toptalStrings} from "../../config/config";
import {SettingsPopoverPage} from "../popovers/settings-popover/settings-popover";
import {PreferencesPopoverPage} from "../popovers/preferences-popover/preferences-popover";
import {NewTaskPopoverPage} from "../popovers/new-task-popover/new-task-popover";
import {TasksProvider} from "../../providers/tasks/tasks";
import {UserProvider} from "../../providers/user/user";
import {Roles} from "../../models/roles";
import {NewUserPopoverPage} from "../popovers/new-user-popover/new-user-popover";
import {AngularFireAuth} from '@angular/fire/auth';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {
  public myRole: Roles;
  public Roles = Roles;
  constructor (public popoverCtrl: PopoverController, private users: UserProvider,
               private taskProvider: TasksProvider, private cd: ChangeDetectorRef,
               private fireAuth: AngularFireAuth) {
    this.taskProvider.processIncomingTasks();
  }

  ionViewDidLoad() {
    this.users.fetchRole().then(role => {
      this.myRole = role;
      if (!this.cd['destroyed']) {
        this.cd.detectChanges();
      }
    });
  }

  public exportAs() {

  }

  public addUser() {
    const popover = this.popoverCtrl.create(NewUserPopoverPage, {}, {cssClass: 'new-task-popover'});
    popover.present();
  }

  public openSettings (myEvent: any): void {
    const list = this.taskProvider.groupedTasks.getValue();
    const popover = this.popoverCtrl.create(SettingsPopoverPage, {
      list: list,
      allowHtml: this.myRole === Roles.USER
    }, {cssClass: 'settings-popover'});
    popover.present({
      ev: myEvent
    });
  }

  public openPreferences (myEvent: any): void {
    const popover = this.popoverCtrl.create(PreferencesPopoverPage, {}, {cssClass: 'preferences-popover'});
    popover.present({
      ev: myEvent
    });
  }

  public createNewTask (): void {
    const uid = this.fireAuth.auth.currentUser.uid;
    const popover = this.popoverCtrl.create(NewTaskPopoverPage, {uid: uid}, {cssClass: 'new-task-popover'});
    popover.present();
  }

  get constants (): any {
    return toptalStrings;
  }

}
