import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HomePage } from './home';
import {SettingsPopoverPageModule} from "../popovers/settings-popover/settings-popover.module";
import {PreferencesPopoverPageModule} from "../popovers/preferences-popover/preferences-popover.module";
import {NewTaskPopoverPageModule} from "../popovers/new-task-popover/new-task-popover.module";
import {EditTaskPopoverPageModule} from "../popovers/edit-task-popover/edit-task-popover.module";
import {TaskManagerPageModule} from "../task-manager/task-manager.module";
import {UserManagerPageModule} from "../user-manager/user-manager.module";
import {NewUserPopoverPageModule} from "../popovers/new-user-popover/new-user-popover.module";
import {AdminManagerPageModule} from "../admin-manager/admin-manager.module";

@NgModule({
  declarations: [
    HomePage,
  ],
  imports: [
    IonicPageModule.forChild(HomePage),
    SettingsPopoverPageModule,
    PreferencesPopoverPageModule,
    NewTaskPopoverPageModule,
    EditTaskPopoverPageModule,
    TaskManagerPageModule,
    UserManagerPageModule,
    NewUserPopoverPageModule,
    AdminManagerPageModule
  ],
})
export class HomePageModule {}
