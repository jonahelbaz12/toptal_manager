webpackJsonp([1],{

/***/ 114:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SettingsPopoverPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_auth_auth__ = __webpack_require__(183);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config_config__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_tasks_tasks__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_social_sharing__ = __webpack_require__(381);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var SettingsPopoverPage = /** @class */ (function () {
    function SettingsPopoverPage(navCtrl, auth, navParams, tasks, social, cd) {
        this.navCtrl = navCtrl;
        this.auth = auth;
        this.navParams = navParams;
        this.tasks = tasks;
        this.social = social;
        this.cd = cd;
        this.loading = false;
        this.allowHtml = false;
    }
    SettingsPopoverPage.prototype.ionViewDidLoad = function () {
        this.allowHtml = this.navParams.get('allowHtml');
    };
    SettingsPopoverPage.prototype.logout = function () {
        this.navCtrl.pop();
        this.auth.logout();
    };
    SettingsPopoverPage.prototype.exportAs = function () {
        var _this = this;
        this.loading = true;
        this.registerChanges();
        var list = this.navParams.get('list');
        this.tasks.getExportedHtml(list).subscribe(function (res) {
            _this.loading = false;
            var options = {
                message: 'Here is an html copy of your logged tasks!',
                subject: 'HTML of Logged Tasks',
                files: [res],
            };
            _this.social.shareWithOptions(options).then(function () {
                _this.registerChanges();
            });
        }, function (err) {
            _this.loading = false;
            _this.registerChanges();
            alert(JSON.stringify(err));
        });
    };
    SettingsPopoverPage.prototype.registerChanges = function () {
        if (!this.cd['destroyed']) {
            this.cd.detectChanges();
        }
    };
    Object.defineProperty(SettingsPopoverPage.prototype, "constants", {
        get: function () {
            return __WEBPACK_IMPORTED_MODULE_3__config_config__["a" /* toptalStrings */];
        },
        enumerable: true,
        configurable: true
    });
    SettingsPopoverPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-settings-popover',template:/*ion-inline-start:"/Users/jonahelbaz/Desktop/jonah-elbaz/toptal-time-managment-app/src/pages/popovers/settings-popover/settings-popover.html"*/'\n<button class="logout-btn" (click)="exportAs()" style="margin-bottom: 10px" *ngIf="allowHtml">\n  {{constants.exportAs}}\n</button>\n<button class="logout-btn" (click)="logout()">\n  {{constants.signout}}\n</button>\n<img *ngIf="loading" src="../../../assets/loader.svg" class="loader"/>\n'/*ion-inline-end:"/Users/jonahelbaz/Desktop/jonah-elbaz/toptal-time-managment-app/src/pages/popovers/settings-popover/settings-popover.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__providers_auth_auth__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_4__providers_tasks_tasks__["a" /* TasksProvider */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_social_sharing__["a" /* SocialSharing */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["k" /* ChangeDetectorRef */]])
    ], SettingsPopoverPage);
    return SettingsPopoverPage;
}());

//# sourceMappingURL=settings-popover.js.map

/***/ }),

/***/ 17:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return toptalStrings; });
var toptalStrings = {
    title: 'Time Manager',
    login: 'Log In',
    signup: 'Sign Up',
    Password: 'Password',
    Username: 'Email',
    RePassword: 'Re-enter Password',
    adminPlaceholder: 'If youre an admin or manager, enter your code',
    signout: 'Sign Out',
    preferredHoursTitle: 'Preferred Hours',
    startTimePlace: 'MMM DD, YYYY 24h',
    startTimeTitle: 'Start Time',
    hoursTitle: 'Total Hours',
    descriptionTitle: 'Task Description',
    newTaskHeader: 'New Task',
    submit: 'Submit',
    emptyDesc: 'A task must have at least a description',
    passwordsDontMatch: 'Your passwords do not match',
    deleteConfirm: 'Are you sure you want to delete this entry?',
    deleteTitle: 'Delete',
    update: 'Update',
    exportAs: 'Export As HTML',
    usersTitle: 'Users',
    oops: 'Oops',
    nothingToEdit: 'Looks like theres not really anything to edit...',
    cantDeleteYourself: 'You can\'t delete yourself',
    cantDeleteUserWithStatus: 'You can\'t delete a user with equal or higher status than yourself',
    newUserHeader: 'New User',
    tempPassword: 'Temporary Password',
    email: 'User Email',
    deleting: 'Delete',
    areYouSure: 'Are you sure you want to delete this user?',
    admin: "Add admin/manager code",
    pendingStatus: "Your elevated status is currently pending",
    rejectedStatus: "Your status request has been rejected. You have been made a regular user. If this is a mistake, please try speaking to an admin."
};
//# sourceMappingURL=config.js.map

/***/ }),

/***/ 180:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Roles; });
var Roles;
(function (Roles) {
    Roles[Roles["MANAGER"] = 0] = "MANAGER";
    Roles[Roles["ADMIN"] = 1] = "ADMIN";
    Roles[Roles["USER"] = 2] = "USER";
})(Roles || (Roles = {}));
//# sourceMappingURL=roles.js.map

/***/ }),

/***/ 181:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EditTaskPopoverPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__config_config__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_tasks_tasks__ = __webpack_require__(58);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var EditTaskPopoverPage = /** @class */ (function () {
    function EditTaskPopoverPage(navCtrl, navParams, tasks) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.tasks = tasks;
        this.newTask = {
            description: "",
            hours: 1,
            dateString: "",
        };
        this.loading = false;
    }
    EditTaskPopoverPage.prototype.ionViewDidLoad = function () {
        this.newTask = this.navParams.get('task');
    };
    EditTaskPopoverPage.prototype.submit = function () {
        var _this = this;
        if (this.tasks.validateTaskEntry(this.newTask)) {
            this.loading = true;
            this.tasks.updateTask(this.newTask, this.navParams.get('uid')).then(function () {
                _this.loading = false;
                _this.navCtrl.pop();
            });
        }
    };
    Object.defineProperty(EditTaskPopoverPage.prototype, "constants", {
        get: function () {
            return __WEBPACK_IMPORTED_MODULE_2__config_config__["a" /* toptalStrings */];
        },
        enumerable: true,
        configurable: true
    });
    EditTaskPopoverPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-edit-task-popover',template:/*ion-inline-start:"/Users/jonahelbaz/Desktop/jonah-elbaz/toptal-time-managment-app/src/pages/popovers/edit-task-popover/edit-task-popover.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>{{constants.newTaskHeader}}</ion-title>\n  </ion-navbar>\n</ion-header>\n<img *ngIf="loading" src="../../../assets/loader.svg" class="loader"/>\n<ion-grid class="main-grid">\n  <ion-row>\n    <ion-col class="desc-col">\n      <ion-label stacked class="desc-title">{{constants.descriptionTitle}}</ion-label>\n      <ion-input type="text" [(ngModel)]="newTask.description" class="desc-input"></ion-input>\n    </ion-col>\n  </ion-row>\n\n  <ion-row>\n    <ion-col class="desc-col">\n      <ion-label stacked class="desc-title">{{constants.hoursTitle}}</ion-label>\n      <ion-input type="number" [(ngModel)]="newTask.hours" class="desc-input"></ion-input>\n    </ion-col>\n    <ion-col>\n      <ion-label stacked class="desc-title">{{constants.startTimeTitle}}</ion-label>\n      <ion-datetime displayFormat="MMM DD, YYYY HH:mm" [(ngModel)]="newTask.dateString" [placeholder]="constants.startTimePlace" class="desc-input"></ion-datetime>\n    </ion-col>\n  </ion-row>\n\n  <ion-row>\n    <ion-col class="submit-task-col">\n      <button class="submit-task-btn" (click)="submit()">\n        {{constants.update}}\n      </button>\n    </ion-col>\n  </ion-row>\n</ion-grid>\n'/*ion-inline-end:"/Users/jonahelbaz/Desktop/jonah-elbaz/toptal-time-managment-app/src/pages/popovers/edit-task-popover/edit-task-popover.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3__providers_tasks_tasks__["a" /* TasksProvider */]])
    ], EditTaskPopoverPage);
    return EditTaskPopoverPage;
}());

//# sourceMappingURL=edit-task-popover.js.map

/***/ }),

/***/ 182:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NewTaskPopoverPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__config_config__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_tasks_tasks__ = __webpack_require__(58);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var NewTaskPopoverPage = /** @class */ (function () {
    function NewTaskPopoverPage(navCtrl, tasks, navParams) {
        this.navCtrl = navCtrl;
        this.tasks = tasks;
        this.navParams = navParams;
        this.newTask = {
            description: "",
            hours: 1,
            dateString: "",
        };
        this.loading = false;
    }
    NewTaskPopoverPage.prototype.submit = function () {
        var _this = this;
        if (this.tasks.validateTaskEntry(this.newTask)) {
            this.loading = true;
            this.tasks.submitNewTask(this.newTask, this.navParams.get('uid')).then(function () {
                _this.loading = false;
                _this.navCtrl.pop();
            });
        }
    };
    Object.defineProperty(NewTaskPopoverPage.prototype, "constants", {
        get: function () {
            return __WEBPACK_IMPORTED_MODULE_2__config_config__["a" /* toptalStrings */];
        },
        enumerable: true,
        configurable: true
    });
    NewTaskPopoverPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-new-task-popover',template:/*ion-inline-start:"/Users/jonahelbaz/Desktop/jonah-elbaz/toptal-time-managment-app/src/pages/popovers/new-task-popover/new-task-popover.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>{{constants.newTaskHeader}}</ion-title>\n  </ion-navbar>\n</ion-header>\n<img *ngIf="loading" src="../../../assets/loader.svg" class="loader"/>\n<ion-grid class="main-grid">\n  <ion-row>\n    <ion-col class="desc-col">\n      <ion-label stacked class="desc-title">{{constants.descriptionTitle}}</ion-label>\n      <ion-input type="text" [(ngModel)]="newTask.description" class="desc-input"></ion-input>\n    </ion-col>\n  </ion-row>\n\n  <ion-row>\n    <ion-col class="desc-col">\n      <ion-label stacked class="desc-title">{{constants.hoursTitle}}</ion-label>\n      <ion-input type="number" [(ngModel)]="newTask.hours" class="desc-input"></ion-input>\n    </ion-col>\n    <ion-col>\n      <ion-label stacked class="desc-title">{{constants.startTimeTitle}}</ion-label>\n      <ion-datetime displayFormat="MMM DD, YYYY HH:mm" [(ngModel)]="newTask.dateString" [placeholder]="constants.startTimePlace" class="desc-input"></ion-datetime>\n    </ion-col>\n  </ion-row>\n\n  <ion-row>\n    <ion-col class="submit-task-col">\n      <button class="submit-task-btn" (click)="submit()">\n        {{constants.submit}}\n      </button>\n    </ion-col>\n  </ion-row>\n</ion-grid>\n'/*ion-inline-end:"/Users/jonahelbaz/Desktop/jonah-elbaz/toptal-time-managment-app/src/pages/popovers/new-task-popover/new-task-popover.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_3__providers_tasks_tasks__["a" /* TasksProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
    ], NewTaskPopoverPage);
    return NewTaskPopoverPage;
}());

//# sourceMappingURL=new-task-popover.js.map

/***/ }),

/***/ 183:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__config_storage__ = __webpack_require__(813);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_fire_auth__ = __webpack_require__(38);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_fire_database__ = __webpack_require__(80);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__config_config__ = __webpack_require__(17);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AuthProvider = /** @class */ (function () {
    function AuthProvider(fireAuth, fireDb) {
        this.fireAuth = fireAuth;
        this.fireDb = fireDb;
    }
    AuthProvider.getLocalUser = function () {
        if (window.localStorage.getItem(__WEBPACK_IMPORTED_MODULE_1__config_storage__["a" /* storage */].local_user) !== null) {
            return (JSON.parse(window.localStorage.getItem(__WEBPACK_IMPORTED_MODULE_1__config_storage__["a" /* storage */].local_user)) || []);
        }
        return null;
    };
    AuthProvider.prototype.validateSignUpData = function (data) {
        if (data.password !== data.repassword) {
            alert(this.constants.passwordsDontMatch);
            return false;
        }
        return true;
    };
    AuthProvider.prototype.signUp = function (data) {
        var _this = this;
        if (data.admin) {
            localStorage.setItem('@toptal:pending_status', JSON.stringify(true));
        }
        return this.fireAuth.auth.createUserWithEmailAndPassword(data.email, data.password)
            .then(function (res) {
            _this.createUser(data.email, data.admin);
        })
            .catch(function (error) {
            localStorage.removeItem('@toptal:pending_status');
            alert(error.message);
        });
    };
    AuthProvider.prototype.attemptLogin = function (data) {
        return this.fireAuth.auth.signInWithEmailAndPassword(data.email, data.password)
            .then(function () {
            return;
        }).catch(function (error) {
            alert(error.message);
            return;
        });
    };
    AuthProvider.prototype.createUser = function (email, admin) {
        var user_id = this.fireAuth.auth.currentUser.uid;
        var obj = {
            email: email,
            admin: admin,
            created_at: new Date().getTime()
        };
        if (obj.admin === "")
            delete obj.admin;
        return this.fireDb.database.ref('users/' + user_id).set(obj)
            .then(function () {
            var user = {
                uid: user_id,
                email: email
            };
            localStorage.setItem('@toptal:user', JSON.stringify(user));
        });
    };
    AuthProvider.prototype.logout = function () {
        localStorage.clear();
        return this.fireAuth.auth.signOut();
    };
    Object.defineProperty(AuthProvider.prototype, "constants", {
        get: function () {
            return __WEBPACK_IMPORTED_MODULE_4__config_config__["a" /* toptalStrings */];
        },
        enumerable: true,
        configurable: true
    });
    AuthProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_fire_auth__["a" /* AngularFireAuth */], __WEBPACK_IMPORTED_MODULE_3__angular_fire_database__["a" /* AngularFireDatabase */]])
    ], AuthProvider);
    return AuthProvider;
}());

//# sourceMappingURL=auth.js.map

/***/ }),

/***/ 185:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__config_config__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__popovers_settings_popover_settings_popover__ = __webpack_require__(114);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__popovers_preferences_popover_preferences_popover__ = __webpack_require__(385);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__popovers_new_task_popover_new_task_popover__ = __webpack_require__(182);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_tasks_tasks__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_user_user__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__models_roles__ = __webpack_require__(180);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__popovers_new_user_popover_new_user_popover__ = __webpack_require__(386);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__angular_fire_auth__ = __webpack_require__(38);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











var HomePage = /** @class */ (function () {
    function HomePage(popoverCtrl, users, taskProvider, cd, fireAuth) {
        this.popoverCtrl = popoverCtrl;
        this.users = users;
        this.taskProvider = taskProvider;
        this.cd = cd;
        this.fireAuth = fireAuth;
        this.Roles = __WEBPACK_IMPORTED_MODULE_8__models_roles__["a" /* Roles */];
        this.taskProvider.processIncomingTasks();
    }
    HomePage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.users.fetchRole().then(function (role) {
            _this.myRole = role;
            if (!_this.cd['destroyed']) {
                _this.cd.detectChanges();
            }
        });
    };
    HomePage.prototype.exportAs = function () {
    };
    HomePage.prototype.addUser = function () {
        var popover = this.popoverCtrl.create(__WEBPACK_IMPORTED_MODULE_9__popovers_new_user_popover_new_user_popover__["a" /* NewUserPopoverPage */], {}, { cssClass: 'new-task-popover' });
        popover.present();
    };
    HomePage.prototype.openSettings = function (myEvent) {
        var list = this.taskProvider.groupedTasks.getValue();
        var popover = this.popoverCtrl.create(__WEBPACK_IMPORTED_MODULE_3__popovers_settings_popover_settings_popover__["a" /* SettingsPopoverPage */], {
            list: list,
            allowHtml: this.myRole === __WEBPACK_IMPORTED_MODULE_8__models_roles__["a" /* Roles */].USER
        }, { cssClass: 'settings-popover' });
        popover.present({
            ev: myEvent
        });
    };
    HomePage.prototype.openPreferences = function (myEvent) {
        var popover = this.popoverCtrl.create(__WEBPACK_IMPORTED_MODULE_4__popovers_preferences_popover_preferences_popover__["a" /* PreferencesPopoverPage */], {}, { cssClass: 'preferences-popover' });
        popover.present({
            ev: myEvent
        });
    };
    HomePage.prototype.createNewTask = function () {
        var uid = this.fireAuth.auth.currentUser.uid;
        var popover = this.popoverCtrl.create(__WEBPACK_IMPORTED_MODULE_5__popovers_new_task_popover_new_task_popover__["a" /* NewTaskPopoverPage */], { uid: uid }, { cssClass: 'new-task-popover' });
        popover.present();
    };
    Object.defineProperty(HomePage.prototype, "constants", {
        get: function () {
            return __WEBPACK_IMPORTED_MODULE_2__config_config__["a" /* toptalStrings */];
        },
        enumerable: true,
        configurable: true
    });
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"/Users/jonahelbaz/Desktop/jonah-elbaz/toptal-time-managment-app/src/pages/home/home.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-row>\n      <ion-col col-2>\n        <img src="../../assets/cog.svg" class="cog-position" (click)="openSettings($event)">\n      </ion-col>\n      <ion-col class="main-title">{{constants.title}}</ion-col>\n      <ion-col col-2 class="dots-position">\n        <img *ngIf="myRole === Roles.USER" src="../../assets/dots.svg" (click)="openPreferences($event)">\n      </ion-col>\n    </ion-row>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n\n  <div *ngIf="myRole === Roles.USER">\n    <page-task-manager></page-task-manager>\n  </div>\n\n  <div *ngIf="myRole === Roles.MANAGER">\n  <page-user-manager></page-user-manager>\n  </div>\n  <div *ngIf="myRole === Roles.ADMIN">\n    <page-admin-manager></page-admin-manager>\n  </div>\n</ion-content>\n\n<ion-fab bottom right class="ion-fab-pos">\n  <ion-icon ion-fab ios="ios-add" md="md-add" style="font-size: 34px"\n            (click)="myRole === Roles.MANAGER || myRole === Roles.ADMIN ? addUser() : createNewTask()"></ion-icon>\n</ion-fab>\n'/*ion-inline-end:"/Users/jonahelbaz/Desktop/jonah-elbaz/toptal-time-managment-app/src/pages/home/home.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* PopoverController */], __WEBPACK_IMPORTED_MODULE_7__providers_user_user__["a" /* UserProvider */],
            __WEBPACK_IMPORTED_MODULE_6__providers_tasks_tasks__["a" /* TasksProvider */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["k" /* ChangeDetectorRef */],
            __WEBPACK_IMPORTED_MODULE_10__angular_fire_auth__["a" /* AngularFireAuth */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 207:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__config_config__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_auth_auth__ = __webpack_require__(183);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AuthPage = /** @class */ (function () {
    function AuthPage(authProvider, cd) {
        this.authProvider = authProvider;
        this.cd = cd;
        this.login = true;
        this.loading = false;
    }
    AuthPage.prototype.loginClicked = function () {
        this.login = true;
        if (!this.cd['destroyed']) {
            this.cd.detectChanges();
        }
    };
    AuthPage.prototype.signupClicked = function () {
        this.login = false;
        if (!this.cd['destroyed']) {
            this.cd.detectChanges();
        }
    };
    AuthPage.prototype.attemptLogin = function (data) {
        var _this = this;
        if (data.email === '' || data.password === '') {
            alert('Please leave no empty fields');
            return;
        }
        this.loading = true;
        if (!this.cd['destroyed']) {
            this.cd.detectChanges();
        }
        this.authProvider.attemptLogin(data).then(function () {
            _this.loading = false;
            if (!_this.cd['destroyed']) {
                _this.cd.detectChanges();
            }
        });
    };
    AuthPage.prototype.attempSignup = function (data) {
        var _this = this;
        if (this.authProvider.validateSignUpData(data)) {
            this.loading = true;
            this.authProvider.signUp(data).then(function () {
                _this.loading = false;
            }, function () {
                _this.loading = false;
            });
        }
    };
    Object.defineProperty(AuthPage.prototype, "constants", {
        get: function () {
            return __WEBPACK_IMPORTED_MODULE_1__config_config__["a" /* toptalStrings */];
        },
        enumerable: true,
        configurable: true
    });
    AuthPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-auth',template:/*ion-inline-start:"/Users/jonahelbaz/Desktop/jonah-elbaz/toptal-time-managment-app/src/pages/auth/auth.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-row>\n      <ion-col class="main-title">{{constants.title}}</ion-col>\n    </ion-row>\n  </ion-navbar>\n</ion-header>\n<img *ngIf="loading" src="../../assets/loader.svg" class="loader"/>\n<ion-content>\n  <div class="segment-location">\n    <segment [leftVal]="constants.login" [rightVal]="constants.signup" (leftClicked)="loginClicked()"\n             (rightClicked)="signupClicked()"></segment>\n  </div>\n\n  <div class="login-boxes" *ngIf="login">\n    <page-login (buttonClick)="attemptLogin($event)"></page-login>\n  </div>\n\n  <div class="login-boxes" *ngIf="!login">\n    <page-sign-up (buttonClick)="attempSignup($event)"></page-sign-up>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/jonahelbaz/Desktop/jonah-elbaz/toptal-time-managment-app/src/pages/auth/auth.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__providers_auth_auth__["a" /* AuthProvider */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["k" /* ChangeDetectorRef */]])
    ], AuthPage);
    return AuthPage;
}());

//# sourceMappingURL=auth.js.map

/***/ }),

/***/ 218:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 218;

/***/ }),

/***/ 262:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/admin-manager/admin-manager.module": [
		263
	],
	"../pages/admin-task-view/admin-task-view.module": [
		383
	],
	"../pages/auth/auth.module": [
		843,
		0
	],
	"../pages/home/home.module": [
		384
	],
	"../pages/login/login.module": [
		399
	],
	"../pages/pending/pending.module": [
		400
	],
	"../pages/popovers/edit-task-popover/edit-task-popover.module": [
		390
	],
	"../pages/popovers/new-task-popover/new-task-popover.module": [
		389
	],
	"../pages/popovers/new-user-popover/new-user-popover.module": [
		393
	],
	"../pages/popovers/preferences-popover/preferences-popover.module": [
		388
	],
	"../pages/popovers/settings-popover/settings-popover.module": [
		387
	],
	"../pages/sign-up/sign-up.module": [
		401
	],
	"../pages/task-manager/task-manager.module": [
		391
	],
	"../pages/user-manager/user-manager.module": [
		392
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 262;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 263:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminManagerPageModule", function() { return AdminManagerPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__admin_manager__ = __webpack_require__(471);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__admin_task_view_admin_task_view_module__ = __webpack_require__(383);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var AdminManagerPageModule = /** @class */ (function () {
    function AdminManagerPageModule() {
    }
    AdminManagerPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__admin_manager__["a" /* AdminManagerPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__admin_manager__["a" /* AdminManagerPage */]),
                __WEBPACK_IMPORTED_MODULE_3__admin_task_view_admin_task_view_module__["AdminTaskViewPageModule"]
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_2__admin_manager__["a" /* AdminManagerPage */]
            ]
        })
    ], AdminManagerPageModule);
    return AdminManagerPageModule;
}());

//# sourceMappingURL=admin-manager.module.js.map

/***/ }),

/***/ 376:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return firebase; });
/* unused harmony export shards */
var firebase = {
    config: {
        apiKey: "AIzaSyCLtfPOAoCKr-vtkCMOx3Nvl8wXHQpz3I4",
        authDomain: "toptal-exam.firebaseapp.com",
        databaseURL: "https://toptal-exam.firebaseio.com",
        projectId: "toptal-exam",
        storageBucket: "toptal-exam.appspot.com",
        messagingSenderId: "978362305440"
    },
};
var shards = {
    users: "https://toptal-exam-users.firebaseio.com/",
    tasks: "https://toptal-exam-tasks.firebaseio.com/",
    main: "https://toptal-exam.firebaseio.com/"
};
//# sourceMappingURL=firebase.js.map

/***/ }),

/***/ 378:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdminTaskViewPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_user_user__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__popovers_edit_task_popover_edit_task_popover__ = __webpack_require__(181);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_tasks_tasks__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__config_config__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__popovers_new_task_popover_new_task_popover__ = __webpack_require__(182);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_fire_auth__ = __webpack_require__(38);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__popovers_settings_popover_settings_popover__ = __webpack_require__(114);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var AdminTaskViewPage = /** @class */ (function () {
    function AdminTaskViewPage(users, navParams, taskProvider, popoverCtrl, alertCtrl, cd, navCtrl, fireAuth) {
        this.users = users;
        this.navParams = navParams;
        this.taskProvider = taskProvider;
        this.popoverCtrl = popoverCtrl;
        this.alertCtrl = alertCtrl;
        this.cd = cd;
        this.navCtrl = navCtrl;
        this.fireAuth = fireAuth;
        this.groupedTasks = [];
        this.loading = false;
        this.workHours = {
            from: "",
            to: ""
        };
    }
    AdminTaskViewPage.prototype.ngOnInit = function () {
        var _this = this;
        this.groupedTasks = [];
        this.user = this.navParams.get('user');
        this.subscribeToTasks();
        this.users.getCustomWorkHours(this.user.uid).then(function (res) {
            _this.workHours = res;
            if (!_this.cd['destroyed']) {
                _this.cd.detectChanges();
            }
        });
        this.taskProvider.submittedNewTask.subscribe(function () {
            _this.subscribeToTasks();
        });
    };
    AdminTaskViewPage.prototype.dismiss = function () {
        this.navCtrl.pop();
    };
    AdminTaskViewPage.prototype.openSettings = function (myEvent) {
        var popover = this.popoverCtrl.create(__WEBPACK_IMPORTED_MODULE_8__popovers_settings_popover_settings_popover__["a" /* SettingsPopoverPage */], {
            list: this.groupedTasks,
            allowHtml: true
        }, { cssClass: 'settings-popover' });
        popover.present({
            ev: myEvent
        });
    };
    AdminTaskViewPage.prototype.isInWorkHours = function (task) {
        return this.users.evaluateCustomWorkHours(task, this.workHours);
    };
    AdminTaskViewPage.prototype.editEntry = function (task) {
        var popover = this.popoverCtrl.create(__WEBPACK_IMPORTED_MODULE_3__popovers_edit_task_popover_edit_task_popover__["a" /* EditTaskPopoverPage */], { task: task, uid: this.user.uid }, { cssClass: 'new-task-popover' });
        popover.present();
    };
    AdminTaskViewPage.prototype.deleteEntry = function (task) {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: this.constants.deleteTitle,
            subTitle: this.constants.deleteConfirm,
            buttons: [
                {
                    text: 'Cancel',
                },
                {
                    text: 'OK',
                    handler: function (data) {
                        _this.taskProvider.deleteEntry(task.uid, _this.user.uid);
                    }
                }
            ]
        });
        alert.present();
    };
    AdminTaskViewPage.prototype.subscribeToTasks = function () {
        var _this = this;
        this.loading = true;
        this.taskProvider.getGroupedTasksById(this.user.uid).then(function (tasks) {
            _this.loading = false;
            _this.groupedTasks = tasks;
            if (!_this.cd['destroyed']) {
                _this.cd.detectChanges();
            }
        });
    };
    AdminTaskViewPage.prototype.createNewTask = function () {
        var popover = this.popoverCtrl.create(__WEBPACK_IMPORTED_MODULE_6__popovers_new_task_popover_new_task_popover__["a" /* NewTaskPopoverPage */], { uid: this.user.uid }, { cssClass: 'new-task-popover' });
        popover.present();
    };
    Object.defineProperty(AdminTaskViewPage.prototype, "myGroupedTasks", {
        get: function () {
            return this.groupedTasks;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AdminTaskViewPage.prototype, "constants", {
        get: function () {
            return __WEBPACK_IMPORTED_MODULE_5__config_config__["a" /* toptalStrings */];
        },
        enumerable: true,
        configurable: true
    });
    AdminTaskViewPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["n" /* Component */])({
            selector: 'page-admin-task-view',template:/*ion-inline-start:"/Users/jonahelbaz/Desktop/jonah-elbaz/toptal-time-managment-app/src/pages/admin-task-view/admin-task-view.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-row>\n      <ion-col col-2 (click)="dismiss()">\n        <span class="exit-btn">x</span>\n      </ion-col>\n      <ion-col class="main-title">{{user.email}}</ion-col>\n      <ion-col col-2 class="dots-position">\n        <img src="../../assets/cog.svg" class="cog-position" (click)="openSettings($event)">\n      </ion-col>\n    </ion-row>\n  </ion-navbar>\n</ion-header>\n\n<img *ngIf="loading" src="../../assets/loader.svg" class="loader"/>\n<ion-content>\n  <ion-list no-lines no-border *ngFor="let group of myGroupedTasks">\n    <ion-item-divider class="title-divider">{{group.titledDate}}</ion-item-divider>\n    <ion-item-sliding *ngFor="let task of group.tasks" class="item-container" no-lines no-border >\n      <ion-item [ngClass]="isInWorkHours(task) ? \'description-work-hours\' : \'description-item\'" no-border no-lines>\n        <ion-row>\n          <ion-col col-8 class="description-col">\n            {{task.description}}\n          </ion-col>\n          <ion-col col-4 text-center class="hours-time-col">\n            <div style="padding-bottom: 10px;"> {{task.hours}}h</div>\n            <div> {{task.time}}</div>\n          </ion-col>\n        </ion-row>\n      </ion-item>\n      <ion-item-options side="right">\n        <button ion-button (click)="editEntry(task)">Edit</button>\n        <button ion-button color="danger" (click)="deleteEntry(task)">Delete</button>\n      </ion-item-options>\n    </ion-item-sliding>\n  </ion-list>\n</ion-content>\n<ion-fab bottom right class="ion-fab-pos">\n  <ion-icon ion-fab ios="ios-add" md="md-add" style="font-size: 34px"\n            (click)="createNewTask()"></ion-icon>\n</ion-fab>\n'/*ion-inline-end:"/Users/jonahelbaz/Desktop/jonah-elbaz/toptal-time-managment-app/src/pages/admin-task-view/admin-task-view.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__providers_user_user__["a" /* UserProvider */], __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["i" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_4__providers_tasks_tasks__["a" /* TasksProvider */], __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["k" /* PopoverController */],
            __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1__angular_core__["k" /* ChangeDetectorRef */],
            __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_7__angular_fire_auth__["a" /* AngularFireAuth */]])
    ], AdminTaskViewPage);
    return AdminTaskViewPage;
}());

//# sourceMappingURL=admin-task-view.js.map

/***/ }),

/***/ 379:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TimeProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var TimeProvider = /** @class */ (function () {
    function TimeProvider() {
    }
    TimeProvider.getTimeOfDayFromTimeString = function (dateString) {
        return dateString.substr(11, 5);
    };
    TimeProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [])
    ], TimeProvider);
    return TimeProvider;
}());

//# sourceMappingURL=time.js.map

/***/ }),

/***/ 383:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminTaskViewPageModule", function() { return AdminTaskViewPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__admin_task_view__ = __webpack_require__(378);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AdminTaskViewPageModule = /** @class */ (function () {
    function AdminTaskViewPageModule() {
    }
    AdminTaskViewPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__admin_task_view__["a" /* AdminTaskViewPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__admin_task_view__["a" /* AdminTaskViewPage */]),
            ],
        })
    ], AdminTaskViewPageModule);
    return AdminTaskViewPageModule;
}());

//# sourceMappingURL=admin-task-view.module.js.map

/***/ }),

/***/ 384:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageModule", function() { return HomePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home__ = __webpack_require__(185);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__popovers_settings_popover_settings_popover_module__ = __webpack_require__(387);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__popovers_preferences_popover_preferences_popover_module__ = __webpack_require__(388);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__popovers_new_task_popover_new_task_popover_module__ = __webpack_require__(389);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__popovers_edit_task_popover_edit_task_popover_module__ = __webpack_require__(390);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__task_manager_task_manager_module__ = __webpack_require__(391);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__user_manager_user_manager_module__ = __webpack_require__(392);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__popovers_new_user_popover_new_user_popover_module__ = __webpack_require__(393);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__admin_manager_admin_manager_module__ = __webpack_require__(263);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};











var HomePageModule = /** @class */ (function () {
    function HomePageModule() {
    }
    HomePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__home__["a" /* HomePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__home__["a" /* HomePage */]),
                __WEBPACK_IMPORTED_MODULE_3__popovers_settings_popover_settings_popover_module__["SettingsPopoverPageModule"],
                __WEBPACK_IMPORTED_MODULE_4__popovers_preferences_popover_preferences_popover_module__["PreferencesPopoverPageModule"],
                __WEBPACK_IMPORTED_MODULE_5__popovers_new_task_popover_new_task_popover_module__["NewTaskPopoverPageModule"],
                __WEBPACK_IMPORTED_MODULE_6__popovers_edit_task_popover_edit_task_popover_module__["EditTaskPopoverPageModule"],
                __WEBPACK_IMPORTED_MODULE_7__task_manager_task_manager_module__["TaskManagerPageModule"],
                __WEBPACK_IMPORTED_MODULE_8__user_manager_user_manager_module__["UserManagerPageModule"],
                __WEBPACK_IMPORTED_MODULE_9__popovers_new_user_popover_new_user_popover_module__["NewUserPopoverPageModule"],
                __WEBPACK_IMPORTED_MODULE_10__admin_manager_admin_manager_module__["AdminManagerPageModule"]
            ],
        })
    ], HomePageModule);
    return HomePageModule;
}());

//# sourceMappingURL=home.module.js.map

/***/ }),

/***/ 385:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PreferencesPopoverPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__config_config__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_user_user__ = __webpack_require__(47);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var PreferencesPopoverPage = /** @class */ (function () {
    function PreferencesPopoverPage(users) {
        this.users = users;
        this.dateStringFrom = this.users.dateStringFrom;
        this.dateStringTo = this.users.dateStringTo;
    }
    PreferencesPopoverPage.prototype.valuesChanged = function () {
        if (this.dateStringFrom !== "" && this.dateStringTo !== "") {
            this.users.writeNewRange(this.dateStringFrom, this.dateStringTo);
        }
    };
    Object.defineProperty(PreferencesPopoverPage.prototype, "constants", {
        get: function () {
            return __WEBPACK_IMPORTED_MODULE_1__config_config__["a" /* toptalStrings */];
        },
        enumerable: true,
        configurable: true
    });
    PreferencesPopoverPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-preferences-popover',template:/*ion-inline-start:"/Users/jonahelbaz/Desktop/jonah-elbaz/toptal-time-managment-app/src/pages/popovers/preferences-popover/preferences-popover.html"*/'\n<ion-header class="preferred-hours-title">\n    {{constants.preferredHoursTitle}}\n</ion-header>\n<ion-grid class="main-grid">\n  <ion-row>\n    <ion-col>\n      <ion-datetime (ionChange)="valuesChanged();" displayFormat="HH:mm" [(ngModel)]="dateStringFrom" class="desc-input"></ion-datetime>\n    </ion-col>\n  </ion-row>\n  <ion-row>\n    <ion-col text-center>\n      TO\n    </ion-col>\n  </ion-row>\n  <ion-row>\n    <ion-col>\n      <ion-datetime [min]="dateStringFrom" (ionChange)="valuesChanged();" displayFormat="HH:mm" [(ngModel)]="dateStringTo" class="desc-input"></ion-datetime>\n    </ion-col>\n  </ion-row>\n</ion-grid>\n'/*ion-inline-end:"/Users/jonahelbaz/Desktop/jonah-elbaz/toptal-time-managment-app/src/pages/popovers/preferences-popover/preferences-popover.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__providers_user_user__["a" /* UserProvider */]])
    ], PreferencesPopoverPage);
    return PreferencesPopoverPage;
}());

//# sourceMappingURL=preferences-popover.js.map

/***/ }),

/***/ 386:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NewUserPopoverPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__config_config__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_user_manager_user_manager__ = __webpack_require__(98);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var NewUserPopoverPage = /** @class */ (function () {
    function NewUserPopoverPage(navCtrl, userManager) {
        this.navCtrl = navCtrl;
        this.userManager = userManager;
        this.tempPass = "";
        this.email = "";
    }
    NewUserPopoverPage.prototype.submit = function () {
        if (this.email === '') {
            alert('Please leave no empty fields');
            return;
        }
        if (this.tempPass.length < 8) {
            alert('Password must be at least 8 character');
            return;
        }
        this.userManager.createNewUser(this.email, this.tempPass);
        this.navCtrl.pop();
    };
    Object.defineProperty(NewUserPopoverPage.prototype, "constants", {
        get: function () {
            return __WEBPACK_IMPORTED_MODULE_2__config_config__["a" /* toptalStrings */];
        },
        enumerable: true,
        configurable: true
    });
    NewUserPopoverPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-new-user-popover',template:/*ion-inline-start:"/Users/jonahelbaz/Desktop/jonah-elbaz/toptal-time-managment-app/src/pages/popovers/new-user-popover/new-user-popover.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>{{constants.newUserHeader}}</ion-title>\n  </ion-navbar>\n</ion-header>\n<img *ngIf="loading" src="../../../assets/loader.svg" class="loader"/>\n<ion-grid class="main-grid">\n  <ion-row>\n    <ion-col class="desc-col">\n      <ion-label stacked class="desc-title">{{constants.email}}</ion-label>\n      <ion-input type="text" [(ngModel)]="email" class="desc-input"></ion-input>\n    </ion-col>\n  </ion-row>\n\n  <ion-row>\n    <ion-col class="desc-col">\n      <ion-label stacked class="desc-title">{{constants.tempPassword}}</ion-label>\n      <ion-input type="password" [(ngModel)]="tempPass" class="desc-input"></ion-input>\n    </ion-col>\n  </ion-row>\n\n\n  <ion-row>\n    <ion-col class="submit-task-col">\n      <button class="submit-task-btn" (click)="submit()">\n        {{constants.submit}}\n      </button>\n    </ion-col>\n  </ion-row>\n</ion-grid>\n'/*ion-inline-end:"/Users/jonahelbaz/Desktop/jonah-elbaz/toptal-time-managment-app/src/pages/popovers/new-user-popover/new-user-popover.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_3__providers_user_manager_user_manager__["a" /* UserManagerProvider */]])
    ], NewUserPopoverPage);
    return NewUserPopoverPage;
}());

//# sourceMappingURL=new-user-popover.js.map

/***/ }),

/***/ 387:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SettingsPopoverPageModule", function() { return SettingsPopoverPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__settings_popover__ = __webpack_require__(114);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SettingsPopoverPageModule = /** @class */ (function () {
    function SettingsPopoverPageModule() {
    }
    SettingsPopoverPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__settings_popover__["a" /* SettingsPopoverPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__settings_popover__["a" /* SettingsPopoverPage */]),
            ],
        })
    ], SettingsPopoverPageModule);
    return SettingsPopoverPageModule;
}());

//# sourceMappingURL=settings-popover.module.js.map

/***/ }),

/***/ 388:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PreferencesPopoverPageModule", function() { return PreferencesPopoverPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__preferences_popover__ = __webpack_require__(385);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var PreferencesPopoverPageModule = /** @class */ (function () {
    function PreferencesPopoverPageModule() {
    }
    PreferencesPopoverPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__preferences_popover__["a" /* PreferencesPopoverPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__preferences_popover__["a" /* PreferencesPopoverPage */]),
            ],
        })
    ], PreferencesPopoverPageModule);
    return PreferencesPopoverPageModule;
}());

//# sourceMappingURL=preferences-popover.module.js.map

/***/ }),

/***/ 389:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewTaskPopoverPageModule", function() { return NewTaskPopoverPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__new_task_popover__ = __webpack_require__(182);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var NewTaskPopoverPageModule = /** @class */ (function () {
    function NewTaskPopoverPageModule() {
    }
    NewTaskPopoverPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__new_task_popover__["a" /* NewTaskPopoverPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__new_task_popover__["a" /* NewTaskPopoverPage */]),
            ],
        })
    ], NewTaskPopoverPageModule);
    return NewTaskPopoverPageModule;
}());

//# sourceMappingURL=new-task-popover.module.js.map

/***/ }),

/***/ 390:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditTaskPopoverPageModule", function() { return EditTaskPopoverPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__edit_task_popover__ = __webpack_require__(181);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var EditTaskPopoverPageModule = /** @class */ (function () {
    function EditTaskPopoverPageModule() {
    }
    EditTaskPopoverPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__edit_task_popover__["a" /* EditTaskPopoverPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__edit_task_popover__["a" /* EditTaskPopoverPage */]),
            ],
        })
    ], EditTaskPopoverPageModule);
    return EditTaskPopoverPageModule;
}());

//# sourceMappingURL=edit-task-popover.module.js.map

/***/ }),

/***/ 391:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TaskManagerPageModule", function() { return TaskManagerPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__task_manager__ = __webpack_require__(817);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var TaskManagerPageModule = /** @class */ (function () {
    function TaskManagerPageModule() {
    }
    TaskManagerPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__task_manager__["a" /* TaskManagerPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__task_manager__["a" /* TaskManagerPage */]),
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_2__task_manager__["a" /* TaskManagerPage */]
            ]
        })
    ], TaskManagerPageModule);
    return TaskManagerPageModule;
}());

//# sourceMappingURL=task-manager.module.js.map

/***/ }),

/***/ 392:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserManagerPageModule", function() { return UserManagerPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__user_manager__ = __webpack_require__(818);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var UserManagerPageModule = /** @class */ (function () {
    function UserManagerPageModule() {
    }
    UserManagerPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__user_manager__["a" /* UserManagerPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__user_manager__["a" /* UserManagerPage */]),
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_2__user_manager__["a" /* UserManagerPage */]
            ]
        })
    ], UserManagerPageModule);
    return UserManagerPageModule;
}());

//# sourceMappingURL=user-manager.module.js.map

/***/ }),

/***/ 393:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewUserPopoverPageModule", function() { return NewUserPopoverPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__new_user_popover__ = __webpack_require__(386);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var NewUserPopoverPageModule = /** @class */ (function () {
    function NewUserPopoverPageModule() {
    }
    NewUserPopoverPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__new_user_popover__["a" /* NewUserPopoverPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__new_user_popover__["a" /* NewUserPopoverPage */]),
            ],
        })
    ], NewUserPopoverPageModule);
    return NewUserPopoverPageModule;
}());

//# sourceMappingURL=new-user-popover.module.js.map

/***/ }),

/***/ 394:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__providers_user_user__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_status_bar__ = __webpack_require__(395);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_splash_screen__ = __webpack_require__(396);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_auth_auth__ = __webpack_require__(207);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_fire_auth__ = __webpack_require__(38);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_home_home__ = __webpack_require__(185);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_network__ = __webpack_require__(397);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_pending_pending__ = __webpack_require__(398);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen, fireAuth, network, toastCtrl, users) {
        var _this = this;
        this.splashScreen = splashScreen;
        this.fireAuth = fireAuth;
        this.network = network;
        this.toastCtrl = toastCtrl;
        this.users = users;
        platform.ready().then(function () {
            statusBar.styleDefault();
            _this.trackAuth();
            _this.trackNetwork();
        });
    }
    MyApp.prototype.trackNetwork = function () {
        var _this = this;
        var disconnectSubscription = this.network.onDisconnect().subscribe(function () {
            var toast = _this.toastCtrl.create({
                message: 'Disconnected From the internet',
                duration: 3000
            });
            toast.present();
        });
    };
    MyApp.prototype.trackAuth = function () {
        var _this = this;
        this.fireAuth.auth.onAuthStateChanged(function (user) {
            _this.splashScreen.hide();
            if (user) {
                if (JSON.parse(localStorage.getItem('@toptal:pending_status'))) {
                    _this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_9__pages_pending_pending__["a" /* PendingPage */]);
                }
                else {
                    _this.users.getRange();
                    _this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_7__pages_home_home__["a" /* HomePage */]);
                }
            }
            else {
                _this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_5__pages_auth_auth__["a" /* AuthPage */]);
            }
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["_9" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* Nav */])
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["n" /* Component */])({template:/*ion-inline-start:"/Users/jonahelbaz/Desktop/jonah-elbaz/toptal-time-managment-app/src/app/app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n'/*ion-inline-end:"/Users/jonahelbaz/Desktop/jonah-elbaz/toptal-time-managment-app/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* Platform */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_splash_screen__["a" /* SplashScreen */], __WEBPACK_IMPORTED_MODULE_6__angular_fire_auth__["a" /* AngularFireAuth */],
            __WEBPACK_IMPORTED_MODULE_8__ionic_native_network__["a" /* Network */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["l" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_0__providers_user_user__["a" /* UserProvider */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 398:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PendingPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__config_config__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__popovers_settings_popover_settings_popover__ = __webpack_require__(114);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_user_user__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__home_home__ = __webpack_require__(185);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var PendingPage = /** @class */ (function () {
    function PendingPage(navCtrl, popoverCtrl, userProvider) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.popoverCtrl = popoverCtrl;
        this.userProvider = userProvider;
        this.userProvider.navigatePending.subscribe(function (res) {
            _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__home_home__["a" /* HomePage */]);
        });
        this.userProvider.observeUserProvider();
    }
    PendingPage.prototype.openSettings = function (myEvent) {
        var popover = this.popoverCtrl.create(__WEBPACK_IMPORTED_MODULE_3__popovers_settings_popover_settings_popover__["a" /* SettingsPopoverPage */], {
            list: null,
            allowHtml: false
        }, { cssClass: 'settings-popover' });
        popover.present({
            ev: myEvent
        });
    };
    Object.defineProperty(PendingPage.prototype, "constants", {
        get: function () {
            return __WEBPACK_IMPORTED_MODULE_2__config_config__["a" /* toptalStrings */];
        },
        enumerable: true,
        configurable: true
    });
    PendingPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-pending',template:/*ion-inline-start:"/Users/jonahelbaz/Desktop/jonah-elbaz/toptal-time-managment-app/src/pages/pending/pending.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-row>\n      <ion-col col-2>\n        <img src="../../assets/cog.svg" class="cog-position" (click)="openSettings($event)">\n      </ion-col>\n      <ion-col class="main-title">{{constants.title}}</ion-col>\n      <ion-col col-2 class="dots-position">\n      </ion-col>\n    </ion-row>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n\n  <div class="pending-status">\n    {{constants.pendingStatus}}\n  </div>\n\n</ion-content>\n'/*ion-inline-end:"/Users/jonahelbaz/Desktop/jonah-elbaz/toptal-time-managment-app/src/pages/pending/pending.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* PopoverController */],
            __WEBPACK_IMPORTED_MODULE_4__providers_user_user__["a" /* UserProvider */]])
    ], PendingPage);
    return PendingPage;
}());

//# sourceMappingURL=pending.js.map

/***/ }),

/***/ 399:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageModule", function() { return LoginPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__login__ = __webpack_require__(820);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var LoginPageModule = /** @class */ (function () {
    function LoginPageModule() {
    }
    LoginPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__login__["a" /* LoginPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__login__["a" /* LoginPage */]),
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_2__login__["a" /* LoginPage */]
            ],
            schemas: [
                __WEBPACK_IMPORTED_MODULE_0__angular_core__["i" /* CUSTOM_ELEMENTS_SCHEMA */]
            ]
        })
    ], LoginPageModule);
    return LoginPageModule;
}());

//# sourceMappingURL=login.module.js.map

/***/ }),

/***/ 400:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PendingPageModule", function() { return PendingPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pending__ = __webpack_require__(398);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var PendingPageModule = /** @class */ (function () {
    function PendingPageModule() {
    }
    PendingPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__pending__["a" /* PendingPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__pending__["a" /* PendingPage */]),
            ],
        })
    ], PendingPageModule);
    return PendingPageModule;
}());

//# sourceMappingURL=pending.module.js.map

/***/ }),

/***/ 401:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignUpPageModule", function() { return SignUpPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__sign_up__ = __webpack_require__(821);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SignUpPageModule = /** @class */ (function () {
    function SignUpPageModule() {
    }
    SignUpPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__sign_up__["a" /* SignUpPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__sign_up__["a" /* SignUpPage */]),
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_2__sign_up__["a" /* SignUpPage */]
            ],
            schemas: [
                __WEBPACK_IMPORTED_MODULE_0__angular_core__["i" /* CUSTOM_ELEMENTS_SCHEMA */]
            ]
        })
    ], SignUpPageModule);
    return SignUpPageModule;
}());

//# sourceMappingURL=sign-up.module.js.map

/***/ }),

/***/ 445:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ComponentsModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__segment_segment__ = __webpack_require__(819);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_app_component__ = __webpack_require__(394);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var ComponentsModule = /** @class */ (function () {
    function ComponentsModule() {
    }
    ComponentsModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [__WEBPACK_IMPORTED_MODULE_1__segment_segment__["a" /* SegmentComponent */]],
            imports: [
                __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_common__["b" /* CommonModule */],
                __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["d" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_5__app_app_component__["a" /* MyApp */]),
            ],
            exports: [__WEBPACK_IMPORTED_MODULE_1__segment_segment__["a" /* SegmentComponent */]]
        })
    ], ComponentsModule);
    return ComponentsModule;
}());

//# sourceMappingURL=components.module.js.map

/***/ }),

/***/ 446:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(447);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(451);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 451:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(396);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(395);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_component__ = __webpack_require__(394);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_auth_auth__ = __webpack_require__(207);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__components_components_module__ = __webpack_require__(445);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_login_login_module__ = __webpack_require__(399);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_sign_up_sign_up_module__ = __webpack_require__(401);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__providers_auth_auth__ = __webpack_require__(183);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__config_firebase__ = __webpack_require__(376);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__angular_fire__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__angular_fire_database__ = __webpack_require__(80);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__angular_fire_auth__ = __webpack_require__(38);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__angular_fire_storage__ = __webpack_require__(839);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_home_home_module__ = __webpack_require__(384);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__providers_tasks_tasks__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__providers_time_time__ = __webpack_require__(379);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__providers_user_user__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__ionic_native_network__ = __webpack_require__(397);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__providers_user_manager_user_manager__ = __webpack_require__(98);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__angular_common_http__ = __webpack_require__(380);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__ionic_native_social_sharing__ = __webpack_require__(381);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__pages_pending_pending_module__ = __webpack_require__(400);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

























var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_6__pages_auth_auth__["a" /* AuthPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/admin-manager/admin-manager.module#AdminManagerPageModule', name: 'AdminManagerPage', segment: 'admin-manager', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/admin-task-view/admin-task-view.module#AdminTaskViewPageModule', name: 'AdminTaskViewPage', segment: 'admin-task-view', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/home/home.module#HomePageModule', name: 'HomePage', segment: 'home', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/auth/auth.module#AuthPageModule', name: 'AuthPage', segment: 'auth', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/pending/pending.module#PendingPageModule', name: 'PendingPage', segment: 'pending', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/popovers/new-task-popover/new-task-popover.module#NewTaskPopoverPageModule', name: 'NewTaskPopoverPage', segment: 'new-task-popover', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/popovers/edit-task-popover/edit-task-popover.module#EditTaskPopoverPageModule', name: 'EditTaskPopoverPage', segment: 'edit-task-popover', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/popovers/preferences-popover/preferences-popover.module#PreferencesPopoverPageModule', name: 'PreferencesPopoverPage', segment: 'preferences-popover', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/popovers/new-user-popover/new-user-popover.module#NewUserPopoverPageModule', name: 'NewUserPopoverPage', segment: 'new-user-popover', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/popovers/settings-popover/settings-popover.module#SettingsPopoverPageModule', name: 'SettingsPopoverPage', segment: 'settings-popover', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/sign-up/sign-up.module#SignUpPageModule', name: 'SignUpPage', segment: 'sign-up', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/task-manager/task-manager.module#TaskManagerPageModule', name: 'TaskManagerPage', segment: 'task-manager', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/user-manager/user-manager.module#UserManagerPageModule', name: 'UserManagerPage', segment: 'user-manager', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_12__angular_fire__["a" /* AngularFireModule */].initializeApp(__WEBPACK_IMPORTED_MODULE_11__config_firebase__["a" /* firebase */].config),
                __WEBPACK_IMPORTED_MODULE_14__angular_fire_auth__["b" /* AngularFireAuthModule */],
                __WEBPACK_IMPORTED_MODULE_13__angular_fire_database__["b" /* AngularFireDatabaseModule */],
                __WEBPACK_IMPORTED_MODULE_15__angular_fire_storage__["a" /* AngularFireStorageModule */],
                __WEBPACK_IMPORTED_MODULE_7__components_components_module__["a" /* ComponentsModule */],
                __WEBPACK_IMPORTED_MODULE_8__pages_login_login_module__["LoginPageModule"],
                __WEBPACK_IMPORTED_MODULE_9__pages_sign_up_sign_up_module__["SignUpPageModule"],
                __WEBPACK_IMPORTED_MODULE_16__pages_home_home_module__["HomePageModule"],
                __WEBPACK_IMPORTED_MODULE_22__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_24__pages_pending_pending_module__["PendingPageModule"]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_6__pages_auth_auth__["a" /* AuthPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["v" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_10__providers_auth_auth__["a" /* AuthProvider */],
                __WEBPACK_IMPORTED_MODULE_17__providers_tasks_tasks__["a" /* TasksProvider */],
                __WEBPACK_IMPORTED_MODULE_18__providers_time_time__["a" /* TimeProvider */],
                __WEBPACK_IMPORTED_MODULE_19__providers_user_user__["a" /* UserProvider */],
                __WEBPACK_IMPORTED_MODULE_20__ionic_native_network__["a" /* Network */],
                __WEBPACK_IMPORTED_MODULE_21__providers_user_manager_user_manager__["a" /* UserManagerProvider */],
                __WEBPACK_IMPORTED_MODULE_23__ionic_native_social_sharing__["a" /* SocialSharing */]
            ],
            schemas: [
                __WEBPACK_IMPORTED_MODULE_1__angular_core__["i" /* CUSTOM_ELEMENTS_SCHEMA */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 47:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_fire_auth__ = __webpack_require__(38);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_fire_database__ = __webpack_require__(80);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__models_roles__ = __webpack_require__(180);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__config_config__ = __webpack_require__(17);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var UserProvider = /** @class */ (function () {
    function UserProvider(fireAuth, fireDb) {
        this.fireAuth = fireAuth;
        this.fireDb = fireDb;
        this.dateStringFrom = "";
        this.dateStringTo = "";
        this.role = __WEBPACK_IMPORTED_MODULE_3__models_roles__["a" /* Roles */].USER;
        this.navigatePending = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* EventEmitter */]();
    }
    UserProvider.prototype.fetchRole = function () {
        var _this = this;
        var current = this.fireAuth.auth.currentUser;
        if (current) {
            return this.fireDb.database.ref('roles/' + current.uid).once('value', function (snapshot) { return snapshot; }).then(function (snapshot) {
                _this.setRole(snapshot.val());
                return _this.role;
            });
        }
        else {
            alert('Not Authenticated');
        }
    };
    UserProvider.prototype.getRange = function () {
        var _this = this;
        var current = this.fireAuth.auth.currentUser;
        if (current) {
            this.fireDb.database.ref('users/' + current.uid + '/range').once('value', function (snapshot) {
                _this.dateStringTo = snapshot.child('to').val() || "";
                _this.dateStringFrom = snapshot.child('from').val() || "";
            });
        }
        else {
            alert('Not Authenticated');
        }
    };
    UserProvider.prototype.writeNewRange = function (from, to) {
        var current = this.fireAuth.auth.currentUser;
        if (current) {
            this.dateStringTo = to;
            this.dateStringFrom = from;
            this.fireDb.database.ref('users/' + current.uid + '/range').update({
                from: this.dateStringFrom,
                to: this.dateStringTo
            });
        }
        else {
            alert('Not Authenticated');
        }
    };
    UserProvider.prototype.isInWorkHours = function (task) {
        return task.time >= this.dateStringFrom && task.time <= this.dateStringTo;
    };
    UserProvider.prototype.evaluateCustomWorkHours = function (task, workHours) {
        return task.time >= workHours.from && task.time <= workHours.to;
    };
    UserProvider.prototype.getCustomWorkHours = function (uid) {
        return this.fireDb.database.ref('users/' + uid + '/range').once('value', function (s) { return s; }).then(function (snapshot) {
            return {
                from: snapshot.child('from').val() || "",
                to: snapshot.child('to').val() || ""
            };
        });
    };
    UserProvider.prototype.observeUserProvider = function () {
        var _this = this;
        var uid = this.fireAuth.auth.currentUser.uid;
        var ref = this.fireDb.database.ref('roles/' + uid);
        ref.on('value', function (snapshot) {
            if (snapshot.exists()) {
                if (snapshot.val() === 'rejected') {
                    alert(__WEBPACK_IMPORTED_MODULE_4__config_config__["a" /* toptalStrings */].rejectedStatus);
                    localStorage.clear();
                    _this.navigatePending.emit(false);
                }
                else {
                    _this.navigatePending.emit(true);
                }
                ref.off();
                localStorage.removeItem('@toptal:pending_status');
            }
        });
    };
    UserProvider.prototype.setRole = function (role) {
        switch (role) {
            case 'manager':
                this.role = __WEBPACK_IMPORTED_MODULE_3__models_roles__["a" /* Roles */].MANAGER;
                return;
            case 'admin':
                this.role = __WEBPACK_IMPORTED_MODULE_3__models_roles__["a" /* Roles */].ADMIN;
                return;
            default:
                this.role = __WEBPACK_IMPORTED_MODULE_3__models_roles__["a" /* Roles */].USER;
        }
    };
    UserProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_fire_auth__["a" /* AngularFireAuth */], __WEBPACK_IMPORTED_MODULE_2__angular_fire_database__["a" /* AngularFireDatabase */]])
    ], UserProvider);
    return UserProvider;
}());

//# sourceMappingURL=user.js.map

/***/ }),

/***/ 471:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdminManagerPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_user_manager_user_manager__ = __webpack_require__(98);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config_config__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__admin_task_view_admin_task_view__ = __webpack_require__(378);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AdminManagerPage = /** @class */ (function () {
    function AdminManagerPage(navCtrl, userManager, cd, alertCtrl, modalCtrl) {
        this.navCtrl = navCtrl;
        this.userManager = userManager;
        this.cd = cd;
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
    }
    AdminManagerPage.prototype.ngOnInit = function () {
        var _this = this;
        this.userManager.fetchUsers();
        this.userManager.users.subscribe(function () {
            if (!_this.cd['destroyed']) {
                _this.cd.detectChanges();
            }
        });
    };
    AdminManagerPage.prototype.editEntry = function (user) {
        var alert = this.alertCtrl.create({
            title: this.constants.oops,
            subTitle: this.constants.nothingToEdit,
            buttons: [
                {
                    text: 'OK',
                }
            ]
        });
        alert.present();
    };
    AdminManagerPage.prototype.deleteEntry = function (user) {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: this.constants.deleting,
            subTitle: this.constants.areYouSure,
            buttons: [
                {
                    text: 'Cancel'
                },
                {
                    text: 'Yes',
                    handler: function () {
                        _this.userManager.attemptToDeleteUser(user, _this.alertCtrl);
                    }
                }
            ]
        });
        alert.present();
    };
    AdminManagerPage.prototype.viewUserTasks = function (user) {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_4__admin_task_view_admin_task_view__["a" /* AdminTaskViewPage */], { user: user });
        modal.present();
    };
    Object.defineProperty(AdminManagerPage.prototype, "users", {
        get: function () {
            return this.userManager.users.getValue();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AdminManagerPage.prototype, "constants", {
        get: function () {
            return __WEBPACK_IMPORTED_MODULE_3__config_config__["a" /* toptalStrings */];
        },
        enumerable: true,
        configurable: true
    });
    AdminManagerPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-admin-manager',template:/*ion-inline-start:"/Users/jonahelbaz/Desktop/jonah-elbaz/toptal-time-managment-app/src/pages/admin-manager/admin-manager.html"*/'<ion-list no-lines no-border>\n  <ion-item-divider class="title-divider">{{constants.usersTitle}}</ion-item-divider>\n  <ion-item-sliding *ngFor="let user of users" class="item-container" no-lines no-border>\n    <ion-item class="description-item" no-border no-lines (click)="viewUserTasks(user)">\n      <ion-row>\n        <ion-col col-8 class="description-col">\n          {{user.email}}\n        </ion-col>\n      </ion-row>\n    </ion-item>\n    <ion-item-options side="right">\n      <button ion-button (click)="editEntry(user)">Edit</button>\n      <button ion-button color="danger" (click)="deleteEntry(user)">Delete</button>\n    </ion-item-options>\n  </ion-item-sliding>\n</ion-list>\n'/*ion-inline-end:"/Users/jonahelbaz/Desktop/jonah-elbaz/toptal-time-managment-app/src/pages/admin-manager/admin-manager.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__providers_user_manager_user_manager__["a" /* UserManagerProvider */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["k" /* ChangeDetectorRef */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* ModalController */]])
    ], AdminManagerPage);
    return AdminManagerPage;
}());

//# sourceMappingURL=admin-manager.js.map

/***/ }),

/***/ 58:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TasksProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__config_config__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_fire_auth__ = __webpack_require__(38);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_fire_database__ = __webpack_require__(80);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__time_time__ = __webpack_require__(379);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_BehaviorSubject__ = __webpack_require__(160);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_BehaviorSubject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_BehaviorSubject__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_common_http__ = __webpack_require__(380);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_operators__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_operators___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_rxjs_operators__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var TasksProvider = /** @class */ (function () {
    function TasksProvider(fireAuth, fireDb, httpClient) {
        this.fireAuth = fireAuth;
        this.fireDb = fireDb;
        this.httpClient = httpClient;
        this.groupedTasks = new __WEBPACK_IMPORTED_MODULE_5_rxjs_BehaviorSubject__["BehaviorSubject"]([]);
        this.submittedNewTask = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* EventEmitter */]();
    }
    TasksProvider.prototype.processIncomingTasks = function () {
        var _this = this;
        var current = this.fireAuth.auth.currentUser;
        if (current) {
            this.fireDb.database.ref('tasks/' + current.uid).on('value', function (snapshot) {
                var newTaskSet = [];
                snapshot.forEach(function (snap) {
                    newTaskSet.push({
                        description: snap.child('description').val(),
                        hours: snap.child('hours').val(),
                        uid: snap.key,
                        dateString: snap.child('dateString').val(),
                        time: __WEBPACK_IMPORTED_MODULE_4__time_time__["a" /* TimeProvider */].getTimeOfDayFromTimeString(snap.child('dateString').val())
                    });
                    return false;
                });
                var sortedGroupTasks = _this.sortAndGroupTasks(newTaskSet);
                _this.groupedTasks.next(sortedGroupTasks);
            });
        }
        else {
            alert('Not Authenticated');
        }
    };
    TasksProvider.prototype.validateTaskEntry = function (task) {
        if (task.description === "") {
            alert(this.constants.emptyDesc);
            return false;
        }
        return true;
    };
    TasksProvider.prototype.submitNewTask = function (task, uid) {
        var _this = this;
        return this.fireDb.database.ref('tasks/' + uid).push(task).then(function () {
            _this.submittedNewTask.emit();
        });
    };
    TasksProvider.prototype.deleteEntry = function (entryId, uid) {
        var _this = this;
        return this.fireDb.database.ref('tasks/' + uid + '/' + entryId).remove().then(function () {
            _this.submittedNewTask.emit();
        });
    };
    TasksProvider.prototype.updateTask = function (task, uid) {
        var _this = this;
        return this.fireDb.database.ref('tasks/' + uid + '/' + task.uid).update(task).then(function () {
            _this.submittedNewTask.emit();
        });
    };
    TasksProvider.prototype.getGroupedTasksById = function (uid) {
        var _this = this;
        return this.fireDb.database.ref('tasks/' + uid).once('value', function (s) { return s; }).then(function (snapshot) {
            var newTaskSet = [];
            snapshot.forEach(function (snap) {
                newTaskSet.push({
                    description: snap.child('description').val(),
                    hours: snap.child('hours').val(),
                    uid: snap.key,
                    dateString: snap.child('dateString').val(),
                    time: __WEBPACK_IMPORTED_MODULE_4__time_time__["a" /* TimeProvider */].getTimeOfDayFromTimeString(snap.child('dateString').val())
                });
                return false;
            });
            return _this.sortAndGroupTasks(newTaskSet);
        });
    };
    TasksProvider.prototype.getExportedHtml = function (list) {
        return this.httpClient.post('http://jonahelbaz.pythonanywhere.com//transform', { jsonToParse: list })
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_7_rxjs_operators__["map"])(function (data) { return data; }));
    };
    TasksProvider.prototype.sortAndGroupTasks = function (tasks) {
        tasks = tasks.sort(function (a, b) {
            var epochA = new Date(a.dateString).getTime();
            var epochB = new Date(b.dateString).getTime();
            return epochB - epochA;
        });
        var groups = [];
        tasks.forEach(function (task) {
            var subDateString = task.dateString.substr(0, 10);
            var i = groups.findIndex(function (o) { return o.titledDate === subDateString; });
            if (i === -1) {
                groups.push({
                    titledDate: subDateString,
                    tasks: [task]
                });
            }
            else {
                groups[i].tasks.push(task);
            }
        });
        return groups;
    };
    Object.defineProperty(TasksProvider.prototype, "constants", {
        get: function () {
            return __WEBPACK_IMPORTED_MODULE_1__config_config__["a" /* toptalStrings */];
        },
        enumerable: true,
        configurable: true
    });
    TasksProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_fire_auth__["a" /* AngularFireAuth */], __WEBPACK_IMPORTED_MODULE_3__angular_fire_database__["a" /* AngularFireDatabase */],
            __WEBPACK_IMPORTED_MODULE_6__angular_common_http__["a" /* HttpClient */]])
    ], TasksProvider);
    return TasksProvider;
}());

//# sourceMappingURL=tasks.js.map

/***/ }),

/***/ 813:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return storage; });
var storage = {
    local_user: '@toptal:user'
};
//# sourceMappingURL=storage.js.map

/***/ }),

/***/ 817:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TaskManagerPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_user_user__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__popovers_edit_task_popover_edit_task_popover__ = __webpack_require__(181);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_tasks_tasks__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__config_config__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_fire_auth__ = __webpack_require__(38);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var TaskManagerPage = /** @class */ (function () {
    function TaskManagerPage(users, taskProvider, popoverCtrl, alertCtrl, cd, fireAuth) {
        this.users = users;
        this.taskProvider = taskProvider;
        this.popoverCtrl = popoverCtrl;
        this.alertCtrl = alertCtrl;
        this.cd = cd;
        this.fireAuth = fireAuth;
    }
    TaskManagerPage.prototype.ngOnInit = function () {
        this.subscribeToTasks();
    };
    TaskManagerPage.prototype.isInWorkHours = function (task) {
        return this.users.isInWorkHours(task);
    };
    TaskManagerPage.prototype.editEntry = function (task) {
        var uid = this.fireAuth.auth.currentUser.uid;
        var popover = this.popoverCtrl.create(__WEBPACK_IMPORTED_MODULE_3__popovers_edit_task_popover_edit_task_popover__["a" /* EditTaskPopoverPage */], { task: task, uid: uid }, { cssClass: 'new-task-popover' });
        popover.present();
    };
    TaskManagerPage.prototype.deleteEntry = function (task) {
        var _this = this;
        var uid = this.fireAuth.auth.currentUser.uid;
        var alert = this.alertCtrl.create({
            title: this.constants.deleteTitle,
            subTitle: this.constants.deleteConfirm,
            buttons: [
                {
                    text: 'Cancel',
                },
                {
                    text: 'OK',
                    handler: function (data) {
                        _this.taskProvider.deleteEntry(task.uid, uid);
                    }
                }
            ]
        });
        alert.present();
    };
    TaskManagerPage.prototype.subscribeToTasks = function () {
        var _this = this;
        this.taskProvider.groupedTasks.subscribe(function () {
            if (!_this.cd['destroyed']) {
                _this.cd.detectChanges();
            }
        });
    };
    Object.defineProperty(TaskManagerPage.prototype, "myGroupedTasks", {
        get: function () {
            return this.taskProvider.groupedTasks.getValue();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TaskManagerPage.prototype, "constants", {
        get: function () {
            return __WEBPACK_IMPORTED_MODULE_5__config_config__["a" /* toptalStrings */];
        },
        enumerable: true,
        configurable: true
    });
    TaskManagerPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["n" /* Component */])({
            selector: 'page-task-manager',template:/*ion-inline-start:"/Users/jonahelbaz/Desktop/jonah-elbaz/toptal-time-managment-app/src/pages/task-manager/task-manager.html"*/'<ion-list no-lines no-border *ngFor="let group of myGroupedTasks">\n  <ion-item-divider class="title-divider">{{group.titledDate}}</ion-item-divider>\n  <ion-item-sliding *ngFor="let task of group.tasks" class="item-container" no-lines no-border >\n    <ion-item [ngClass]="isInWorkHours(task) ? \'description-work-hours\' : \'description-item\'" no-border no-lines>\n      <ion-row>\n        <ion-col col-8 class="description-col">\n          {{task.description}}\n        </ion-col>\n        <ion-col col-4 text-center class="hours-time-col">\n          <div style="padding-bottom: 10px;"> {{task.hours}}h</div>\n          <div> {{task.time}}</div>\n        </ion-col>\n      </ion-row>\n    </ion-item>\n    <ion-item-options side="right">\n      <button ion-button (click)="editEntry(task)">Edit</button>\n      <button ion-button color="danger" (click)="deleteEntry(task)">Delete</button>\n    </ion-item-options>\n  </ion-item-sliding>\n</ion-list>\n'/*ion-inline-end:"/Users/jonahelbaz/Desktop/jonah-elbaz/toptal-time-managment-app/src/pages/task-manager/task-manager.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__providers_user_user__["a" /* UserProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers_tasks_tasks__["a" /* TasksProvider */], __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["k" /* PopoverController */],
            __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1__angular_core__["k" /* ChangeDetectorRef */],
            __WEBPACK_IMPORTED_MODULE_6__angular_fire_auth__["a" /* AngularFireAuth */]])
    ], TaskManagerPage);
    return TaskManagerPage;
}());

//# sourceMappingURL=task-manager.js.map

/***/ }),

/***/ 818:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserManagerPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_user_manager_user_manager__ = __webpack_require__(98);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config_config__ = __webpack_require__(17);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var UserManagerPage = /** @class */ (function () {
    function UserManagerPage(navCtrl, userManager, cd, alertCtrl) {
        this.navCtrl = navCtrl;
        this.userManager = userManager;
        this.cd = cd;
        this.alertCtrl = alertCtrl;
    }
    UserManagerPage.prototype.ngOnInit = function () {
        var _this = this;
        this.userManager.fetchUsers();
        this.userManager.users.subscribe(function () {
            if (!_this.cd['destroyed']) {
                _this.cd.detectChanges();
            }
        });
    };
    UserManagerPage.prototype.editEntry = function (user) {
        var alert = this.alertCtrl.create({
            title: this.constants.oops,
            subTitle: this.constants.nothingToEdit,
            buttons: [
                {
                    text: 'OK',
                }
            ]
        });
        alert.present();
    };
    UserManagerPage.prototype.deleteEntry = function (user) {
        this.userManager.attemptToDeleteUser(user, this.alertCtrl);
    };
    Object.defineProperty(UserManagerPage.prototype, "users", {
        get: function () {
            return this.userManager.users.getValue();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UserManagerPage.prototype, "constants", {
        get: function () {
            return __WEBPACK_IMPORTED_MODULE_3__config_config__["a" /* toptalStrings */];
        },
        enumerable: true,
        configurable: true
    });
    UserManagerPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-user-manager',template:/*ion-inline-start:"/Users/jonahelbaz/Desktop/jonah-elbaz/toptal-time-managment-app/src/pages/user-manager/user-manager.html"*/'<ion-list no-lines no-border>\n  <ion-item-divider class="title-divider">{{constants.usersTitle}}</ion-item-divider>\n  <ion-item-sliding *ngFor="let user of users" class="item-container" no-lines no-border >\n    <ion-item class="description-item" no-border no-lines>\n      <ion-row>\n        <ion-col col-8 class="description-col">\n          {{user.email}}\n        </ion-col>\n      </ion-row>\n    </ion-item>\n    <ion-item-options side="right">\n      <button ion-button (click)="editEntry(user)">Edit</button>\n      <button ion-button color="danger" (click)="deleteEntry(user)">Delete</button>\n    </ion-item-options>\n  </ion-item-sliding>\n</ion-list>\n'/*ion-inline-end:"/Users/jonahelbaz/Desktop/jonah-elbaz/toptal-time-managment-app/src/pages/user-manager/user-manager.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__providers_user_manager_user_manager__["a" /* UserManagerProvider */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["k" /* ChangeDetectorRef */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], UserManagerPage);
    return UserManagerPage;
}());

//# sourceMappingURL=user-manager.js.map

/***/ }),

/***/ 819:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SegmentComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var SegmentComponent = /** @class */ (function () {
    function SegmentComponent() {
        this.leftClicked = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* EventEmitter */]();
        this.rightClicked = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* EventEmitter */]();
        this.leftValOn = true;
    }
    SegmentComponent.prototype.clickedRight = function () {
        this.leftValOn = false;
        this.rightClicked.emit(null);
    };
    SegmentComponent.prototype.clickedLeft = function () {
        this.leftValOn = true;
        this.leftClicked.emit(null);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", String)
    ], SegmentComponent.prototype, "leftVal", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", String)
    ], SegmentComponent.prototype, "rightVal", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["P" /* Output */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* EventEmitter */])
    ], SegmentComponent.prototype, "leftClicked", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["P" /* Output */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* EventEmitter */])
    ], SegmentComponent.prototype, "rightClicked", void 0);
    SegmentComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'segment',template:/*ion-inline-start:"/Users/jonahelbaz/Desktop/jonah-elbaz/toptal-time-managment-app/src/components/segment/segment.html"*/'<ion-grid>\n  <ion-row class="segment-row">\n    <ion-col (click)="this.clickedLeft()" [ngClass]="leftValOn ? \'segment-clicked\' : \'segment-off\'">\n      {{leftVal}}\n    </ion-col>\n    <ion-col (click)="this.clickedRight()"\n             [ngClass]="!leftValOn ? \'segment-clicked\' : \'segment-off\'">\n      {{rightVal}}\n    </ion-col>\n  </ion-row>\n</ion-grid>\n'/*ion-inline-end:"/Users/jonahelbaz/Desktop/jonah-elbaz/toptal-time-managment-app/src/components/segment/segment.html"*/
        }),
        __metadata("design:paramtypes", [])
    ], SegmentComponent);
    return SegmentComponent;
}());

//# sourceMappingURL=segment.js.map

/***/ }),

/***/ 820:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__config_config__ = __webpack_require__(17);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var LoginPage = /** @class */ (function () {
    function LoginPage() {
        this.email = "";
        this.password = "";
        this.buttonClick = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* EventEmitter */]();
    }
    Object.defineProperty(LoginPage.prototype, "constants", {
        get: function () {
            return __WEBPACK_IMPORTED_MODULE_1__config_config__["a" /* toptalStrings */];
        },
        enumerable: true,
        configurable: true
    });
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["P" /* Output */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* EventEmitter */])
    ], LoginPage.prototype, "buttonClick", void 0);
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-login',template:/*ion-inline-start:"/Users/jonahelbaz/Desktop/jonah-elbaz/toptal-time-managment-app/src/pages/login/login.html"*/'<div>\n  <ion-input [(ngModel)]="email" [placeholder]="constants.Username" class="common-input username-input"></ion-input>\n  <ion-input [(ngModel)]="password" [placeholder]="constants.Password" class="common-input pass-input" type="password"></ion-input>\n</div>\n\n<div class="login-div">\n  <button class="login-button" (click)="buttonClick.emit({email: email, password: password})">\n    {{constants.login}}\n  </button>\n</div>\n\n'/*ion-inline-end:"/Users/jonahelbaz/Desktop/jonah-elbaz/toptal-time-managment-app/src/pages/login/login.html"*/,
        }),
        __metadata("design:paramtypes", [])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 821:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SignUpPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__config_config__ = __webpack_require__(17);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var SignUpPage = /** @class */ (function () {
    function SignUpPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.email = "";
        this.password = "";
        this.repassword = "";
        this.admin = "";
        this.buttonClick = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* EventEmitter */]();
    }
    SignUpPage.prototype.clickedSignUp = function () {
        this.buttonClick.emit({
            email: this.email,
            password: this.password,
            repassword: this.repassword,
            admin: this.admin
        });
    };
    Object.defineProperty(SignUpPage.prototype, "constants", {
        get: function () {
            return __WEBPACK_IMPORTED_MODULE_2__config_config__["a" /* toptalStrings */];
        },
        enumerable: true,
        configurable: true
    });
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["P" /* Output */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* EventEmitter */])
    ], SignUpPage.prototype, "buttonClick", void 0);
    SignUpPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-sign-up',template:/*ion-inline-start:"/Users/jonahelbaz/Desktop/jonah-elbaz/toptal-time-managment-app/src/pages/sign-up/sign-up.html"*/'<div>\n  <ion-input [(ngModel)]="email" [placeholder]="constants.Username" class="common-input username-input"></ion-input>\n  <ion-input [(ngModel)]="password" [placeholder]="constants.Password" class="common-input pass-input" type="password"></ion-input>\n  <ion-input [(ngModel)]="repassword" [placeholder]="constants.RePassword" class="common-input pass-input" type="password"></ion-input>\n  <ion-input [(ngModel)]="admin" [placeholder]="constants.admin" class="common-input pass-input" type="password"></ion-input>\n</div>\n\n<div class="login-div">\n  <button class="login-button" (click)="clickedSignUp()">\n    {{constants.signup}}\n  </button>\n</div>\n\n'/*ion-inline-end:"/Users/jonahelbaz/Desktop/jonah-elbaz/toptal-time-managment-app/src/pages/sign-up/sign-up.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
    ], SignUpPage);
    return SignUpPage;
}());

//# sourceMappingURL=sign-up.js.map

/***/ }),

/***/ 98:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserManagerProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_fire_database__ = __webpack_require__(80);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config_config__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_fire_auth__ = __webpack_require__(38);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__config_firebase__ = __webpack_require__(376);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_firebase__ = __webpack_require__(808);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_firebase__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__user_user__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__models_roles__ = __webpack_require__(180);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var UserManagerProvider = /** @class */ (function () {
    function UserManagerProvider(fireDb, fireAuth, userProvider) {
        this.fireDb = fireDb;
        this.fireAuth = fireAuth;
        this.userProvider = userProvider;
        this.users = new __WEBPACK_IMPORTED_MODULE_1_rxjs__["BehaviorSubject"]([]);
    }
    UserManagerProvider.prototype.createNewUser = function (email, password) {
        var _this = this;
        var secondaryApp = __WEBPACK_IMPORTED_MODULE_6_firebase__["initializeApp"](__WEBPACK_IMPORTED_MODULE_5__config_firebase__["a" /* firebase */].config, "Secondary");
        return secondaryApp.auth().createUserWithEmailAndPassword(email, password)
            .then(function (res) {
            secondaryApp.auth().signOut();
            secondaryApp.delete();
            return _this.fireDb.database.ref('users/' + res.user.uid).set({
                email: email,
                created_at: new Date().getTime()
            });
        }).catch(function (error) {
            alert(error.message);
        });
    };
    UserManagerProvider.prototype.fetchUsers = function () {
        var _this = this;
        this.fireDb.database.ref('users').on('value', function (snapshot) {
            var users = [];
            snapshot.forEach(function (snap) {
                users.push({
                    email: snap.child('email').val(),
                    uid: snap.key
                });
            });
            _this.users.next(users);
        });
    };
    UserManagerProvider.prototype.attemptToDeleteUser = function (user, alertCtrl) {
        var _this = this;
        var current = this.fireAuth.auth.currentUser;
        if (current.uid === user.uid) {
            this.alertError(alertCtrl, this.constants.cantDeleteYourself);
            return;
        }
        this.doesUserHaveRole(user.uid).then(function (res) {
            if (res === null || res === undefined) {
                _this.deleteAllUserEntries(user);
                return;
            }
            if (_this.userProvider.role === __WEBPACK_IMPORTED_MODULE_8__models_roles__["a" /* Roles */].MANAGER && res !== null) {
                _this.alertError(alertCtrl, _this.constants.cantDeleteUserWithStatus);
                return;
            }
            if (_this.userProvider.role === __WEBPACK_IMPORTED_MODULE_8__models_roles__["a" /* Roles */].ADMIN && res === 'admin') {
                _this.alertError(alertCtrl, _this.constants.cantDeleteUserWithStatus);
                return;
            }
            _this.deleteAllUserEntries(user);
        });
    };
    UserManagerProvider.prototype.deleteAllUserEntries = function (user) {
        var _this = this;
        return this.fireDb.database.ref('tasks/' + user.uid).remove().then(function () {
            _this.fireDb.database.ref('users/' + user.uid).remove();
        });
    };
    UserManagerProvider.prototype.doesUserHaveRole = function (uid) {
        return this.fireDb.database.ref('roles/' + uid).once('value', function (snap) { return snap; }).then(function (snapshot) {
            return snapshot.val();
        });
    };
    UserManagerProvider.prototype.alertError = function (alertCtrl, subTitle) {
        var alert = alertCtrl.create({
            title: this.constants.oops,
            subTitle: subTitle,
            buttons: [
                {
                    text: 'OK',
                }
            ]
        });
        alert.present();
    };
    Object.defineProperty(UserManagerProvider.prototype, "constants", {
        get: function () {
            return __WEBPACK_IMPORTED_MODULE_3__config_config__["a" /* toptalStrings */];
        },
        enumerable: true,
        configurable: true
    });
    UserManagerProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_fire_database__["a" /* AngularFireDatabase */], __WEBPACK_IMPORTED_MODULE_4__angular_fire_auth__["a" /* AngularFireAuth */],
            __WEBPACK_IMPORTED_MODULE_7__user_user__["a" /* UserProvider */]])
    ], UserManagerProvider);
    return UserManagerProvider;
}());

//# sourceMappingURL=user-manager.js.map

/***/ })

},[446]);
//# sourceMappingURL=main.js.map